/**
 * Created by Micheal on 16/10/15.
 */
$(function(){
    if($('#lt_sel_prize').html()==''){
        $('#sliderange').hide();
    }
    var maxp = '',minp='',maxpot=0;
    $('#lt_sel_dyprize option').each(function(){
        var vals = $(this).val();
        var vval= vals.split('|');
        if(vval[1]>0){
            minp=vval[0];
            maxpot =vval[1]*100
        }else maxp=vval[0];
    });
    $('#rangevalue').html(maxp+'/0%');
    $('#prizerange').attr('max',maxpot);
    $('#prizerange').change(function() {
        if( $(this).val()>0){
            $('#rangevalue').html( minp+'/'+$(this).val()+'%' );
            $('#lt_sel_dyprize').empty().append('<option value="'+minp+'|'+($(this).val()/100).toFixed(3)+'"  selected>');
        }if($(this).val()==0){
            $('#rangevalue').html( maxp+'/'+$(this).val()+'%' );
            $('#lt_sel_dyprize').empty().append('<option value="'+maxp+'|'+$(this).val()+'"  selected>');
        }
    });
})