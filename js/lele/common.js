mui.init();//必须
//所有a链接如果需要跳转 就给a添加class J-href；
mui(document).on("tap", ".J-href", function() {
	mui.openWindow({
		url: this.href
	});
})
//线路按钮绑定事件
mui(document).on('tap', '.abtnhover-group a', function() {
	$(this).addClass("mui-active")
})

//关闭弹出框
mui(document).on('tap','.return-btn', function() {
	mui(".mui-popover").popover('hide');
})

//玩法的select 选择事件
mui(document).on('change','.betTypeWrap select', function() {
	var indexs=this.selectedIndex;
	var t=this.options[indexs].innerText;
	$(this).parent().find("span").text(t)
})
