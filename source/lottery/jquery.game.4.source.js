/*
* lottery
*
* version: 2.0.0 (12/13/2010)
* @ jQuery v1.3 or later ,suggest use 1.4
*
* Copyright 2012
*
*/
;
(function($){//start

    //check the version, need 1.3 or later , suggest use 1.4
    if (/^1.2/.test($.fn.jquery) || /^1.1/.test($.fn.jquery)) {
	alert('requires jQuery v1.3 or later!  You are using v' + $.fn.jquery);
	return;
    }
    $(document).ready(function(){
	$.playInit({
	    data_label: face,
	    data_prize: pri_user_data,
	    cur_issue : pri_cur_issue,
            last_open : pri_lastopen,
	    issues    : pri_issues,
	    issuecount: pri_issuecount,
	    servertime: pri_servertime,
	    lotteryid : pri_lotteryid,
	    isdynamic : pri_isdynamic,
	    ajaxurl   : pri_ajaxurl
	});
    });
    $.playInit = function(opts){//整个购彩界面的初始化
	var ps = {//整个JS的初试化默认参数
	    data_label : [],
	    data_prize : [],
	    data_id : {
		id_cur_issue    : '#current_issue',//装载当前期的ID
		id_cur_end      : '#current_endtime',//装载当前期结束时间的ID
		id_cur_sale	: '#current_sale',	//已销售期数ID
		id_cur_left	: '#current_left',  //今日还剩多少期ID
		id_count_down   : '#count_down',//装载倒计时的ID
		id_labelbox     : '#tabbar-div-s2', //装载大标签的元素ID
		id_smalllabel   : '#tabbar-div-s3',//装载小标签的元素ID
		id_desc         : '#lt_desc',//装载玩法描述的ID
		id_help         : '#lt_help',//玩法帮助
                id_example      : '#lt_example',//玩法帮助
		id_selector     : '#lt_selector',//装载选号区的ID
		id_sel_num      : '#lt_sel_nums',//装载选号区投注倍数的ID
		id_sel_money    : '#lt_sel_money',//装载选号区投注金额的ID
		id_sel_times    : '#lt_sel_times',//选号区倍数输入框ID
                id_reduce_times : '#reducetime',//减少倍数
                id_plus_times   : '#plustime',//增加倍数
		id_sel_insert   : '#lt_sel_insert',//添加按钮
		id_sel_modes    : '#lt_sel_modes',//元角模式选择
		id_sel_prize	: '#lt_sel_prize',//动态奖金设置ID
		id_cf_count     : '#lt_cf_count', //统计投注单数
		id_cf_clear     : '#lt_cf_clear', //清空确认区数据的按钮ID
		id_cf_content   : '#lt_cf_content',//装载确认区数据的TABLE的ID
		id_cf_num       : '#lt_cf_nums',//装载确认区总投注注数的ID
		id_cf_money     : '#lt_cf_money',//装载确认区总投注金额的ID
		id_cf_help	: '#lt_cf_help',//投注内容帮助
		id_issues       : '#lt_issues',//装载起始期的ID
		id_sendok       : '#lt_buy',  //立即购买按钮
		id_tra_if       : '#lt_trace_if',//是否追号的checkbox ID
		id_tra_stop     : '#lt_trace_stop',//是否追中即停的checkbox ID
		id_tra_box     : '#lt_trace_box',//装载整个追号内容的ID，主要是隐藏和显示
		id_tra_alct     : '#lt_trace_alcount',//装载可追号期数的ID
		id_tra_label    : '#lt_trace_label',//装载同倍，翻倍，利润追号等元素的ID
		id_tra_lhtml    : '#lt_trace_labelhtml',//装载同倍翻倍等标签所表示的内容
		id_tra_ok       : '#lt_trace_ok',//立即生成按钮
		id_tra_issues   : '#lt_trace_issues',//装载追号的一系列期数的ID
                id_random_one   : '#lt_random_one', //随机一注
                id_random_five  : '#lt_random_five'//随机五注
	    },
            //当前期
	    cur_issue : {
		issue:'20100210-001',
		endtime:'2010-02-10 09:10:00',
		opentime:'2011-02-10 09:10:00'
	    },
            last_open : {
                issue:'20100210-001',
                code:'12345',
                endtime:'2010-02-10 09:10:00',
                opentime:'2011-02-10 09:10:00'
            },
	    issues    : {},//所有的可追号期数集合
	    servertime : '2011-02-10 09:09:40',//服务器时间[与服务器同步]
	    ajaxurl    : '',    //提交的URL地址,获取下一期的地址是后面加上flag=read,提交是后面加上flag=save
	    lotteryid  : 1,//彩种ID
	    isdynamic	 : 1,//是否开启动态奖金支持
	    ontimeout  : function(){},//时间结束后执行的函数
	    onfinishbuy: function(){},//购买成功后调用的函数
	    test : ''
	}
	opts = $.extend( {}, ps, opts || {} ); //根据参数初始化默认配置
	/*************************************全局参数配置 **************************/
	$.extend({
	    lt_id_data : opts.data_id,
	    lt_method_data : {},//当前所选择的玩法数据
	    lt_method : methods,
	    lt_issues : opts.issues,//所有的可追号期的初始集合
	    lt_issuecount : opts.issuecount,//每天可追号总期数
	    lt_ajaxurl: opts.ajaxurl,
	    lt_lottid : opts.lotteryid,
	    lt_isdyna : opts.isdynamic,
	    lt_total_nums : 0,//总投注注数
	    lt_total_money: 0,//总投注金额[非追号]
	    lt_time_leave : 0, //本期剩余时间
	    lt_time_open  : 0, //刚完的一期剩余开奖时间
	    lt_open_time  : opts.cur_issue.opentime,
	    lt_end_time  : opts.cur_issue.endtime,
	    lt_open_status: true,
            lt_last_open: opts.last_open,
	    lt_same_code  : [],//用于限制一个方法里不能投相同单
	    lt_ontimeout  : opts.ontimeout,
	    lt_onfinishbuy: opts.onfinishbuy,
	    lt_trace_base : 0,//追号的基本金额.
	    lt_submiting  : false,//是否正在提交表单
	    lt_ismargin	 : true, //是否支持利润率追号
	    lt_prizes   : [], //投注内容的奖金情况
	    lt_dantuo   : false,//是否是胆拖玩法
            lt_danlen   : 0 //胆码个数
	});
	ps = null;
	opts.data_id = null;
	opts.issues  = null;
	opts.ajaxurl = null;
	opts.lotteryid = null;
	if( $.browser.msie ){//&& /MSIE 6.0/.test(navigator.userAgent)
            $($.lt_id_data.id_tra_if).show();
	    CollectGarbage();//释放内存
	}
        //重新根据用户权限获取奖金设置及动态奖金分配情况
	var noRightMethod = [];//记录没有权限的玩法
	var haveRight = false;//是否有菜单的权限
	$.each(opts.data_label, function(i,n){//玩法组
	    noRightMethod = [];
	    $.each(n.label, function(j,m){//玩法
		haveRight = false;
		for(k in opts.data_prize){
		    if(m.methodid==opts.data_prize[k].methodid){
			m.prize = opts.data_prize[k].prize;
			m.dyprize = opts.data_prize[k].dyprize;
			haveRight = true;
			break;
		    }
		}
		if(haveRight == false){
		    noRightMethod.push(m);
		}
	    })
	    for(var ll = 0;ll < noRightMethod.length; ll++){
		opts.data_label[i].label.remove(noRightMethod[ll]);//删除没有权限的玩法
	    }
	});
	//开始倒计时
	$($.lt_id_data.id_count_down).lt_timer(opts.servertime,opts.cur_issue.endtime);
    if($.lt_last_open.statuscode < 1 && $.lt_last_open.issue != "" && $.lt_open_status == true){//定时获取开奖号码
        $("#lt_opentimeleft").lt_opentimer($.lt_last_open.endtime,$.lt_last_open.opentime,$.lt_last_open.issue);
    }
	var bhtml = ''; //大标签HTML
	var postion = 0;
	$.each(opts.data_label, function(i,n){//玩法组标签
        if(n.label.length>0){
            if(typeof(n)=='object'){
                n.isdefault = n.isdefault ? 1 : 0;
                if(n.isdefault == 1){//第一个标签自动选择或者选择默认玩法群
                    bhtml += '<option selected value="'+i+'">'+n.title+'</option>';
                var default_mg='';
                default_mg += '<i class="fa fa-chevron-down"></i> <span>'+n.title+'</span>';
                $('#default_mg').html(default_mg);
                lt_smalllabel({//生成该标签下的小标签
                    title:n.title,
                    label:n.label
                });
                }else{
                if(i==2){
                	var default_mg='';
                    default_mg += '<i class="fa fa-chevron-down"></i><span>'+n.title+'</span>';
                    $('#default_mg').html(default_mg);
                    lt_smalllabel({//生成该标签下的小标签
                        title:n.title,
                        label:n.label
                    });
                }
                bhtml += '<option value="'+i+'" >'+n.title+'</option>';
                }
            }
            postion++;
        }
	});
	$bhtml = $(bhtml);
	$($.lt_id_data.id_labelbox).empty();
	$(bhtml).appendTo($.lt_id_data.id_labelbox);
	$($.lt_id_data.id_labelbox).change(function(){//切换玩法组
	    var index = parseInt($(this).attr("value"),10);
	    lt_smalllabel({
		title:opts.data_label[index].title,
		label:opts.data_label[index].label
	    });
	});
	//生成并写入起始期内容
	var chtml = '<select name="lt_issue_start" id="lt_issue_start">';
	var j = 0;
	var endtime = 0;
	var currentendtime = new Date($.lt_end_time.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();;
	$.each($.lt_issues,function(i,n){
	    endtime = new Date(n.endtime.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
	    if(j<$.lt_issuecount && endtime>=currentendtime){
		j++;
		chtml += '<option value="'+n.issue+'">'+n.issue+(n.issue==opts.cur_issue.issue?lot_lang.dec_s7:'')+'</option>';
	    }
	});
	chtml += '</select><input type="hidden" name="lt_total_nums" id="lt_total_nums" value="0"><input type="hidden" name="lt_total_money" id="lt_total_money" value="0">';
	$(chtml).appendTo($.lt_id_data.id_issues);
	//确认区事件
	$("tr",$($.lt_id_data.id_cf_content)).live("mouseover",function(){//确认区行颜色变化效果
	    $(this).addClass("on");
	}).live("mouseout",function(){
	    $(this).removeClass("on");
	});
	$($.lt_id_data.id_cf_clear).click(function(){//清空按钮
	    $.confirm(lot_lang.am_s5,function(){
		$.lt_total_nums  = 0;//总注数清零
		$.lt_total_money = 0;//总金额清零
		$.lt_trace_base  = 0;//追号金额基数清零
		$.lt_same_code   = [];//已在确认区的数据
		$($.lt_id_data.id_cf_num).html(0);//显示数据清零
		$($.lt_id_data.id_cf_money).html(0);//显示数据清零
		$($.lt_id_data.id_cf_count).html(0);//总投注项清零
		$($.lt_id_data.id_cf_content).children().empty();
		$('<tr class="nr"><td class="tl_li_l" width="4"></td><td colspan="6" class="noinfo">暂无投注项</td><td class="tl_li_rn" width="4"></td></tr>').prependTo($.lt_id_data.id_cf_content);
		cleanTraceIssue();//清空追号区数据
		if( $.lt_ismargin == false ){
		    traceCheckMarginSup();
		}
	    });
	});
	$($.lt_id_data.id_cf_help).mouseover(function(){
	    var $h = $('<div class=ibox><table border=0 cellspacing=0 cellpadding=0><tr class=t><td class=tl></td><td class=tm></td><td class=tr></td></tr><tr class=mm><td class=ml><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td>'+lot_lang.am_s35+'</td><td class=mr><img src="'+pri_imgserver+'/images/comm/t.gif"></td></tr><tr class=b><td class=bl></td><td class=bm><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td class=br> </td></tr></table><div class=ar><div class=ic></div></div></div>');
	    var offset = $(this).offset();
	    var left = offset.left-37;
	    var top  = offset.top-51;
	    $(this).openFloat($h,"more",left,top);
	}).mouseout(function(){
	    $(this).closeFloat();
	});
        //玩法帮助说明
        $($.lt_id_data.id_help).mouseover(function(){
	    var $h = $('<div class=ibox><table border=0 cellspacing=0 cellpadding=0><tr class=t><td class=tl></td><td class=tm></td><td class=tr></td></tr><tr class=mm><td class=ml><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td>'+$.lt_method_data.methodhelp+'</td><td class=mr><img src="'+pri_imgserver+'/images/comm/t.gif"></td></tr><tr class=b><td class=bl></td><td class=bm><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td class=br> </td></tr></table><div class=ar><div class=ic></div></div></div>');
	    var offset = $(this).offset();
	    var left = offset.left-37;
	    var top  = offset.top-35;
	    $(this).openFloat($h,"more",left,top);
	}).mouseout(function(){
	    $(this).closeFloat();
	});
        //中奖说明
        $($.lt_id_data.id_example).mouseover(function(){
	    var $h = $('<div class=ibox><table border=0 cellspacing=0 cellpadding=0><tr class=t><td class=tl></td><td class=tm></td><td class=tr></td></tr><tr class=mm><td class=ml><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td>'+$.lt_method_data.methodexample+'</td><td class=mr><img src="'+pri_imgserver+'/images/comm/t.gif"></td></tr><tr class=b><td class=bl></td><td class=bm><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td class=br> </td></tr></table><div class=ar><div class=ic></div></div></div>');
	    var offset = $(this).offset();
	    var left = offset.left-37;
	    var top  = offset.top-35;
	    $(this).openFloat($h,"more",left,top);
	}).mouseout(function(){
	    $(this).closeFloat();
	});
	//追号区
	$($.lt_id_data.id_tra_if).lt_trace({
	    issues:opts.issues
	});
	//确认投注按钮事件
	$($.lt_id_data.id_sendok).lt_ajaxSubmit();
    }
    //动态载入小标签
    var lt_smalllabel = function(opts){
	var ps = {
	    title:'',
	    label:[]
	};    //标签数据
	opts   = $.extend( {}, ps, opts || {} ); //根据参数初始化默认配置
	var html = '';
	var dyhtml = '';
	$.each(opts.label, function(i,n){
	    if(typeof(n)=='object'){
		if( i == 0){//第一个标签自动选择
		    // html += '<div class="act"><span class="tab-front" id="smalllabel_'+i+'">'+n.desc+'</span></div>';
            html = '<option class="method-tab-front" id="smalllabel_'+i+'">'+n.desc+'</option>';

            var default_mg='';
            default_mg += '<i class="fa fa-chevron-down"></i><span>'+n.title+'</span>';
            $('#default_mg').html(default_mg);

		    lt_selcountback();//选号区的统计归零
		    $.lt_method_data = {
			methodid : n.methodid,
			title: opts.title,
			name : n.name,
			str  : n.show_str,
			prize: n.prize,
			dyprize: n.dyprize,
			modes: $.lt_method_data.modes ? $.lt_method_data.modes : {},
			sp   : n.code_sp,
                        methodhelp : n.methodhelp,
                        methoddesc : n.methoddesc,
                        methodexample : n.methodexample,
			maxcodecount: n.maxcodecount
		    };

		    $($.lt_id_data.id_selector).lt_selectarea(n.selectarea);//生成选号界面
		    //生成模式选择
		    selmodes  = getCookie("modes");
		    $($.lt_id_data.id_sel_modes).empty();
		    $.each(n.modes,function(j,m){
			$.lt_method_data.modes[m.modeid]= {
			    name:m.name,
			    rate:Number(m.rate)
			};
			addItem($($.lt_id_data.id_sel_modes)[0],''+m.name+'',m.modeid);
		    });
		    SelectItem($($.lt_id_data.id_sel_modes)[0],selmodes);
		    //生成动态返点
		    dypoint = getCookie("dypoint");
		    $($.lt_id_data.id_sel_prize).empty();
            $($.lt_id_data.id_sel_times).val(getCookie('times'));
		    if( n.dyprize.length >= 1 && $.lt_isdyna==1 ){
			dyhtml = '<SELECT name="lt_sel_dyprize" id="lt_sel_dyprize" style="margin-left: 1px;padding: 0px;width:80px;">';
			$.each(n.dyprize[0].prize,function(j,m){
			    dyhtml += '<OPTION value="'+m.prize+'|'+m.point+'"'+(m.point<=0.1 ? ' selected' : '')+'>'+m.prize+'-'+(Math.ceil(m.point*1000)/10)+'%</OPTION>';
			});
			dyhtml += '</SELECT>';
			$($.lt_id_data.id_sel_prize).html(lot_lang.dec_s34);
			$(dyhtml).appendTo($.lt_id_data.id_sel_prize);
        if(window.prizeChangeBind) prizeChangeBind();
		    }
		}else{
		    // html += '<div class="back"><span class="tab-back" id="smalllabel_'+i+'">'+n.desc+'</span></div>';
            html += '<option id="smalllabel_'+i+'">'+n.desc+'</option>';
		}
	    }
	});
	$html = $(html);
	$($.lt_id_data.id_smalllabel).empty();
	$html.appendTo($.lt_id_data.id_smalllabel);
	if( opts.label.length == 1 ){
            $($.lt_id_data.id_smalllabel).empty();
            $($.lt_id_data.id_smalllabel).parent().hide();
        }else{
            $($.lt_id_data.id_smalllabel).parent().show();
        }
	// $("span[id^='smalllabel_']:first",$($.lt_id_data.id_smalllabel)).attr("class","tab-front").data("ischecked",'yes');//第一个标签自动选择[兼容各种浏览器]
        $($.lt_id_data.id_smalllabel).change(function(){

        if( $("option:selected", this).data("ischecked") == 'yes' ){//如果已经选择则无任何动作
		return;
	    }
	    var index = parseInt($("option:selected", this).attr("id").replace("smalllabel_",""),10);
	    var tmpopts = opts;
	    lt_selcountback();//选号区的统计归零
	    $.lt_method_data = {
		methodid : tmpopts.label[index].methodid,
		title: tmpopts.title,
		name : tmpopts.label[index].name,
		str  : tmpopts.label[index].show_str,
		prize: tmpopts.label[index].prize,
		dyprize: tmpopts.label[index].dyprize,
		modes: $.lt_method_data.modes ? $.lt_method_data.modes : {},
		sp   : tmpopts.label[index].code_sp,
                methoddesc : tmpopts.label[index].methoddesc,
                methodhelp : tmpopts.label[index].methodhelp,
                methodexample : tmpopts.label[index].methodexample,
		maxcodecount : tmpopts.label[index].maxcodecount
	    };

	    $($.lt_id_data.id_smalllabel).removeData("ischecked");

	    $(this).data("ischecked",'yes'); //标记为已选择

	    $($.lt_id_data.id_selector).lt_selectarea(tmpopts.label[index].selectarea);//生成选号界面
	    //生成模式选择
	    $($.lt_id_data.id_sel_modes).empty();
	    selmodes  = getCookie("modes");
	    $.each(tmpopts.label[index].modes,function(j,m){
		$.lt_method_data.modes[m.modeid]= {
		    name:m.name,
		    rate:Number(m.rate)
		    };
		addItem($($.lt_id_data.id_sel_modes)[0],''+m.name+'',m.modeid);
	    });
	    SelectItem($($.lt_id_data.id_sel_modes)[0],selmodes);
	    //生成动态返点
	    dypoint = getCookie("dypoint");
	    $($.lt_id_data.id_sel_prize).empty();
	    if( tmpopts.label[index].dyprize.length == 1 && $.lt_isdyna==1 ){
		dyhtml = '<SELECT name="lt_sel_dyprize" id="lt_sel_dyprize" style="margin-left: 1px;padding: 0px;width:80px;">';
		$.each(tmpopts.label[index].dyprize[0].prize,function(j,m){
		    dyhtml += '<OPTION value="'+m.prize+'|'+m.point+'"'+(m.point<=0.1 ? ' selected' : '')+'>'+m.prize+'-'+(Math.ceil(m.point*1000)/10)+'%</OPTION>';
		});
		dyhtml += '</SELECT>';
		$($.lt_id_data.id_sel_prize).html(lot_lang.dec_s34);
		$(dyhtml).appendTo($.lt_id_data.id_sel_prize);
	    }
	});
    };

    var lt_selcountback = function(){
	$($.lt_id_data.id_sel_times).val(1);
	$($.lt_id_data.id_sel_money).html(0);
	$($.lt_id_data.id_sel_num).html(0);
    };

    //选号区动态插入函数，可能是手动编辑
    $.fn.lt_selectarea = function( opts ){
	var ps = {//默认参数
	    type   : 'digital', //选号，'input':输入型,'digital':数字选号型,'dxds':大小单双类型
	    layout : [
	    {
		title:'百位',
		no:'0|1|2|3|4|5|6|7|8|9',
		place:0,
		cols:1
	    },
	    {
		title:'十位',
		no:'0|1|2|3|4|5|6|7|8|9',
		place:1,
		cols:1
	    },
	    {
		title:'个位',
		no:'0|1|2|3|4|5|6|7|8|9',
		place:2,
		cols:1
	    }
	    ],//数字型的号码排列
	    noBigIndex : 5,    //前面多少个号码是小号,即大号是从多少个以后开始的
	    isButton   : true,  //是否需要全大小奇偶清按钮
	    isDanTuo   : false //是否是胆拖玩法
	};
	opts = $.extend( {}, ps, opts || {} ); //根据参数初始化默认配置
	var data_sel = [];//用户已选择或者已输入的数据
	var minchosen = [];//每一位上最少选择的号码个数
	var max_place= 0; //总共的选择型排列数
	var otype = opts.type.toLowerCase();    //类型全部转换为小写
	var methodname = $.lt_method[$.lt_method_data.methodid];//玩法的简写,如:'ZX3'
	var html  = '';
	if( otype == 'input' ){//输入型，则载入输入型的数据
	    var tempdes    = '';
	    switch( methodname ){
		default      :
		    tempdes = lot_lang.dec_s4;
		    break;
	    }
	    html += '<div class="nbs single"><table class=ha><tr><td valign=top><textarea id="lt_write_box" style="width:600px;height:80px;"></textarea><br />'+tempdes+'</td><td valign=top><span class=ds><span class=lsbb><input name="lt_write_del" type="button" value="删除重复号" class="lsb" id="lt_write_del"></span></span><span class=ds><span class=lsbb><input name="lt_write_import" type="button" value="&nbsp;导入文件&nbsp;" class="lsb" id="lt_write_import"></span></span><span class=ds><span class=lsbb><input name="lt_write_empty" type="button" value="&nbsp;清&nbsp;&nbsp;空&nbsp;" class="lsb" id="lt_write_empty"></span></span></td></tr></table></div>';
	    data_sel[0] = []; //初始数据
	    tempdes = null;
	}else if( otype == 'digital' || otype == 'digitalts'){//数字选号型
	    $.each(opts.layout, function(i,n){
		if(typeof(n)=='object'){
		    n.place  = parseInt(n.place,10);
		    max_place = n.place > max_place ? n.place : max_place;
		    data_sel[n.place] = [];//初始数据
		    minchosen[n.place] = (typeof(n.minchosen) == 'undefined') ? 1 : n.minchosen;//初始每一位上最少选择的号码个数
		    html += '<div class="numRow flex">';
		    if( n.cols > 0 ){//有标题
			html += '<div class="flex-2"><div class=ballName>';
			if( n.title.length > 0 ){
			    html += n.title;
			}
			html += '</div></div>';
		    }else{
                html += '<div class=tiempty></div>';
            }
            if(otype == 'digitalts'){
                html += '<div class="flex-11">';
            }else{
                html += '<div class="flex-11">';
            }
		    numbers = n.no.split("|");
		    j = numbers.length;
            if(methodname=='STHTX' || methodname=='STHDX' || methodname=='ETHFX' || methodname=='SLTHTX'){
                var codestyle='';
                var ballstyle ='';
                codestyle='width:90px;margin-right:20px;';
                ballstyle='width:90px;border-radius:10%';
            }
		    for( i=0; i<j; i++ ){
			html += '<div class="ballGroup" style="'+codestyle+'"><div class="ballNum" style="'+ballstyle+'" name="lt_place_'+n.place+'">'+numbers[i]+'</div></div>';
		    }
		    html += '</div>';
		    if( opts.isButton == true ){
			if((!opts.isDanTuo) || (opts.isDanTuo && n.place == 1)){
			    html += '<div class=to><ul><li class="l"></li><li class="dxjoq" name="all">'+lot_lang.bt_sel_all+'</li><li class="dxjoq" name="big">'+lot_lang.bt_sel_big+'</li><li class="dxjoq" name="small">'+lot_lang.bt_sel_small+'</li><li class="dxjoq" name="odd">'+lot_lang.bt_sel_odd+'</li><li class="dxjoq" name="even">'+lot_lang.bt_sel_even+'</li><li class="dxjoq" name="clean">'+lot_lang.bt_sel_clean+'</li><li class="r"></li></ul></div>';
			}
		    }
		    html += '</div>';
		}
	    });
	}
	html += '<div class="c"></div>';
	if(opts.isDanTuo){
            $.lt_dantuo = true;//胆拖玩法
            $.lt_danlen = parseInt(methodname.substring(methodname.length-1),10);
        }else{
            $.lt_dantuo = false;//非胆拖玩法
        }
	$html = $(html)
	$(this).empty();
	$html.appendTo(this);
        $($.lt_id_data.id_desc).html($.lt_method_data.methoddesc);
	var me = this;
	var _SortNum = function(a,b){//数字大小排序
	    if( otype != 'input' ){
                a = a.replace(/111/gi,1).replace(/222/gi,2).replace(/333/gi,3).replace(/444/gi,4).replace(/555/gi,5).replace(/666/gi,6);
                if(methodname != "KSHZ" && methodname != "SBTHHZ"){//不是和值
                    a = a.replace(/11/gi,1).replace(/22/gi,2).replace(/33/gi,3).replace(/44/gi,4).replace(/55/gi,5).replace(/66/gi,6);
                }
                a = a.replace(/\*/gi,"");
                b = b.replace(/111/gi,1).replace(/222/gi,2).replace(/333/gi,3).replace(/444/gi,4).replace(/555/gi,5).replace(/666/gi,6);
                if(methodname != "KSHZ" && methodname != "SBTHHZ"){//不是和值
                    b = b.replace(/11/gi,1).replace(/22/gi,2).replace(/33/gi,3).replace(/44/gi,4).replace(/55/gi,5).replace(/66/gi,6);
                }
                b = b.replace(/\*/gi,"");
	    }
	    a = parseInt(a,10);
	    b = parseInt(b,10);
	    if( isNaN(a) || isNaN(b) ){
		return true;
	    }
	    return (a-b);
	};
	/************************ 验证号码合法性以及计算单笔投注注数以及金额 ***********************/
	//===================输入型检测===================
        //二不同号单式合法号码检测，合法返回TRUE，非法返回FALSE,n号码，len号码长度
	var _EBTHDScheck = function(n,len){
            if (len != 2){
                return false
            }
            var first = "";
            var second = "";
            var i = 0;
            for(i=0;i<len;i++){
                if (i == 0){
                    first = n.substr(i, 1);
                }
                if (i == 1){
                    second = n.substr(i, 1);
                }
            }
            if (first == second){
                return false;
            }
            return true;
        };
        //三不同号单式合法号码检测，合法返回TRUE，非法返回FALSE,n号码，len号码长度
        var _SBTHDScheck = function(n,len){
            if (len != 3){
                return false
            }
            var first = "";
            var second = "";
            var third = "";
            var i = 0;
            for(i=0;i<len;i++){
                if (i == 0){
                    first = n.substr(i, 1);
                }
                if (i == 1){
                    second = n.substr(i, 1);
                }
                if (i == 2){
                    third = n.substr(i, 1);
                }

            }
            if (first == second || second == third || third == first){
                return false;
            }
            else
            {
                return true;
            }
            return false;
        };
        //二同号单选单式合法号码检测，合法返回TRUE，非法返回FALSE,n号码，len号码长度
	var _ETHDXcheck = function(n,len){
            if (len != 3){
                return false
            }
            var first = "";
            var second = "";
            var third = "";
            var i = 0;
            for(i=0;i<len;i++){
                if (i == 0){
                    first = n.substr(i, 1);
                }
                if (i == 1){
                    second = n.substr(i, 1);
                }
                if (i == 2){
                    third = n.substr(i, 1);
                }

            }
            if (first == second && second == third){
                return false;
            }
            if (first == second || second == third || third == first){
                return true;
            }
            return false;
        };
	//号码检测,l:号码长度,e是否返回非法号码，true是,false返回合法注数,fun对每个号码的附加检测,sort:是否对每个号码排序
	var _inputCheck_Num = function(l,e,fun,sort){
	    var nums = data_sel[0].length;
	    var error= [];
	    var newsel=[];
	    var partn= "";
	    l = parseInt(l,10);
	    switch(l){
                case 2 :
		    partn= /^[1-6]{2}$/;
		    break;
		case 3 :
                    partn= /^[1-6]{3}$/;
                    break;
		default:
		    partn= /^[1-6]{3}$/;
		    break;
	    }
	    fun = $.isFunction(fun) ? fun : function(s){
		return true;
	    };
	    $.each(data_sel[0],function(i,n){
		n = $.trim(n);
		if( partn.test(n) && fun(n,l) ){//合格号码
		    if( sort ){
			if( n.indexOf(" ") == -1 ){
			    n = n.split("");
			    n.sort(_SortNum);
			    n = n.join("");
			}else{
			    n = n.split(" ");
			    n.sort(_SortNum);
			    n = n.join(" ");
			}
		    }
		    data_sel[0][i] = n;
		    newsel.push(n);
		}else{//不合格则注数减
		    if( n.length>0 ){
			error.push(n);
		    }
		    nums = nums - 1;
		}
	    });
	    if( e == true ){
		data_sel[0] = newsel;
		return error;
	    }
	    return nums;
	};
	function checkNum(){//时时计算投注注数与金额等
	    var nums  = 0, mname = $.lt_method[$.lt_method_data.methodid];//玩法的简写,如:'ZX3'
	    var modes = parseInt($($.lt_id_data.id_sel_modes).val(),10);//投注模式
	    //01:验证号码合法性并计算注数
	    if( otype == 'input' ){//输入框形式的检测
		if( data_sel[0].length > 0 ){//如果输入的有值
		    switch(mname){
                        case 'SBTH'  ://三不同号单式
                            nums = _inputCheck_Num(3,false,_SBTHDScheck,true);
                            break;
                        case 'EBTH'  ://二不同号单式
                            nums = _inputCheck_Num(2,false,_EBTHDScheck,true);
                            break;
                        case 'ETHDX' ://二同号单选
                            nums = _inputCheck_Num(3,false,_ETHDXcheck,true);
                            break;
                        default   :
			    break;
		    }
		}
	    }else{//其他选择号码形式[暂时就数字型和大小单双]
		var tmp_nums = 1;
		switch(mname){//根据玩法分类不同做不同处理
		    case 'SBTH': //三不同号
                        if( data_sel[0].length >= 3 ){ //C(n,m)
                            nums += Combination(data_sel[0].length, 3);
                        }
                        break;
		    case 'SBTHDT'://三不同号胆拖
			var danlen = data_sel[0].length;//胆码
                        var tuolen = data_sel[1].length;//拖码
                        if(danlen < 1 || tuolen < 1 || danlen >= 3){
                            nums = 0;
                        }else{
                            nums = Combination(tuolen,3-danlen);
                        }
			break;
                    case 'SBTHHZ'://三不同号和值
                        var cc = {
                            6:1,
                            7:1,
                            8:2,
                            9:3,
                            10:3,
                            11:3,
                            12:3,
                            13:2,
                            14:1,
                            15:1
                        };
                       var s = data_sel[0].length;
                        if(s > 0){
                            for( j=0; j<s; j++ ){
				nums += cc[parseInt(data_sel[0][j],10)];
			    }
                        }
                        break;
                    case 'EBTH': //二不同号
                        if( data_sel[0].length >= 2 ){ //C(n,m)
                            nums += Combination(data_sel[0].length, 2);
                        }
                        break;
		    default     : //默认情况
			for( i=0; i<=max_place; i++ ){
			    if( data_sel[i].length == 0 ){//有位置上没有选择
				tmp_nums = 0;
				break;
				break;
			    }
			    tmp_nums *= data_sel[i].length;
			}
			nums = tmp_nums;
			break;
		}
	    }
	    //03:计算金额
	    var times = parseInt($($.lt_id_data.id_sel_times).val(),10);
	    if( isNaN(times) )
	    {
		times = 1;
		$($.lt_id_data.id_sel_times).val(1);
	    }
	    var money = Math.round(times * nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//倍数*注数*单价 * 模式
	    money = isNaN(money) ? 0 : money;
	    $($.lt_id_data.id_sel_num).html(nums);   //写入临时的注数
	    $($.lt_id_data.id_sel_money).html(money);//写临时单笔价格
	};
	//重复号处理
//	var dumpNum = function(isdeal){
//	    var l   = data_sel[0].length;
//	    var err = [];
//	    var news= []; //除去重复号后的结果
//	    if( l == 0 ){
//		return err;
//	    }
//	    for( i=0; i<l; i++ ){
//		if( $.inArray(data_sel[0][i],err) != -1 ){
//		    continue;
//		}
//		for( j=i+1; j<l; j++ ){
//		    if( data_sel[0][i] == data_sel[0][j] ){
//			err.push(data_sel[0][i]);
//			break;
//		    }
//		}
//		news.push(data_sel[0][i]);
//	    }
//	    if( isdeal ){//如果是做删除重复号的处理
//		data_sel[0] = news;
//	    }
//	    return err;
//	};
    var dumpNum = function(isdeal){
        var l = data_sel[0].length;
        var unique = {},newer = [],err = []; // 不重复号码数组及重复的号码数组
        for(var i = 0; i < l; i++) {
            if(unique[data_sel[0][i]]) err.push(data_sel[0][i]);
            unique[data_sel[0][i]] = 1;
        }
        if(isdeal) { // 如果是做删除重复号的处理
            var j = 0;
            for(var k in unique) {
                newer[j] = k;
                j++;
            }
            data_sel[0] = newer;
        }
        return err;
    }

	//输入框的字符串处理
	function _inptu_deal(){
	    var s = $.trim($("#lt_write_box",$(me)).val());
	    s     = $.trim(s.replace(/[^\s\r,;，；　１２３４５６1-6]/g,""));
	    var m = s;
	    switch( methodname ){
		default      :
		    s = s.replace(/[\s\r,;，；　]/g,"|").replace(/(\|)+/g,"|");
		    break;
	    }
	    s = s.replace(/１/g,"1").replace(/２/g,"2").replace(/３/g,"3").replace(/４/g,"4").replace(/５/g,"5").replace(/６/g,"6");
	    if( s == "" ){
		data_sel[0] = []; //清空数据
	    }else{
		data_sel[0] = s.split("|");
	    }
	    return m;
	};
	/************************ 事件触发处理 ****************************/
	if( otype == 'input' ){//手动输入型处理
	    $("#lt_write_del",$(me)).click(function(){//删除重复号
		var err = dumpNum(true);
		if( err.length > 0 ){//如果有重复号码
		    checkNum();
		    switch( methodname ){
			default      :
			    $("#lt_write_box",$(me)).val(data_sel[0].join(" "));
			    $.alert('<div class="datainfo">'+lot_lang.am_s3+'\r'+err.join(" ")+'\r&nbsp;</div>','','',400);
			    break;
		    }
		}else{
		    $.alert(lot_lang.am_s4);
		}
	    });
	    $("#lt_write_import",$(me)).click(function(){//载入文件处理
		$.ajaxUploadUI({
		    title    : lot_lang.dec_s27,
		    url      : './js/dialog/fileupload.php',//服务端处理的文件
		    loadok   : lot_lang.dec_s28,
		    filetype : ['txt','csv'],//允许载入的文件类型
		    success  : function(data){
			$("#lt_write_box",$(me)).val(data).change();
		    },//数据处理
		    onfinish : function(){
			$("#lt_write_box",$(me)).focus();
		    }
		});
	    });
	    $("#lt_write_box",$(me)).change(function(){//输入框时时变动处理
		var s = _inptu_deal();
		$(this).val(s);
		checkNum();
	    }).keyup(function(){
		_inptu_deal();
		checkNum();
	    });
	    $("#lt_write_empty",$(me)).click(function(){//清空处理
		data_sel[0] = []; //清空数据
		$("#lt_write_box",$(me)).val("");
		checkNum();
	    });
	}

	//选中号码处理
	function selectNum( obj, isButton ){
	    if( $.trim($(obj).attr("class")) == 'selected' ){//如果本身是已选中，则不做任何处理
		return;
	    }
	    $(obj).attr("class","selected");//样式改变为选中
	    place = Number($(obj).attr("name").replace("lt_place_",""));
	    var number = $.trim($(obj).html());
	    //替换号码中的无用字符
            number = number.replace(/\<span.*\<\/span>/gi,"").replace(/\r\n/gi,"");
            number = number.replace(/\<div.*>(.*)\<\/div>/gi,"$1").replace(/\r\n/gi,"");
            number = number.replace(/111/gi,1).replace(/222/gi,2).replace(/333/gi,3).replace(/444/gi,4).replace(/555/gi,5).replace(/666/gi,6);
            if(methodname != "KSHZ" && methodname != "SBTHHZ"){//不是和值
                number = number.replace(/11/gi,1).replace(/22/gi,2).replace(/33/gi,3).replace(/44/gi,4).replace(/55/gi,5).replace(/66/gi,6);
            }
            number = number.replace(/\*/gi,"");
            if(methodname == "EBTHDT" || methodname == "SBTHDT"){
                var danlen = 1;
                if(methodname == "SBTHDT"){
                    danlen = 2;
                }
                if(data_sel[0].length+1 > danlen && place == 0){
                    var lastnumber = data_sel[0][data_sel[0].length-1];
                    $.each($('div[name="lt_place_0"][class="on"]'),function(i,n){
                        if($(this).html() == lastnumber){
                            $(this).attr("class","");//样式改变为未选中
                            data_sel[0] = $.grep(data_sel[0],function(n,i){//从选中数组中删除取消的号码
                                return n == lastnumber;
                            },true);
                        }
                    });
                }
            }
            if(methodname == "ETHDX" || methodname == "EBTHDT" || methodname == "SBTHDT"){
                var unplace = 1-place;
                if(data_sel[unplace].contains(number)){
                    $.each($('div[name="lt_place_'+unplace+'"][class="on"]'),function(i,n){
                        var tmphtml = $(this).html()
                        if(methodname == "ETHDX"){
                            tmphtml = tmphtml.replace(/11/gi,1).replace(/22/gi,2).replace(/33/gi,3).replace(/44/gi,4).replace(/55/gi,5).replace(/66/gi,6);
                        }
                        if(tmphtml == number){
                            $(this).attr("class","");//样式改变为未选中
                            data_sel[unplace] = $.grep(data_sel[unplace],function(n,i){//从选中数组中删除取消的号码
                                return n == number;
                            },true);
                        }
                    });
                }
            }
	    data_sel[place].push(number);//加入到数组中
	    if( !isButton ){//如果不是按钮触发则进行统计，按钮的统一进行统计
		checkNum();
	    }
	};
	//取消选中号码处理
	function unSelectNum( obj, isButton ){
	    if( $.trim($(obj).attr("class")) != 'selected' ){//如果本身是未选中，则不做任何处理
		return;
	    }
	    $(obj).attr("class","ballNum");//样式改变为未选中
	    place = Number($(obj).attr("name").replace("lt_place_",""));
	    var number = $.trim($(obj).html());
            number = number.replace(/\<span.*\<\/span>/gi,"").replace(/\r\n/gi,"");
            number = number.replace(/\<div.*>(.*)\<\/div>/gi,"$1").replace(/\r\n/gi,"");
            number = number.replace(/111/gi,1).replace(/222/gi,2).replace(/333/gi,3).replace(/444/gi,4).replace(/555/gi,5).replace(/666/gi,6);
            if(methodname != "KSHZ" && methodname != "SBTHHZ"){//不是和值
                number = number.replace(/11/gi,1).replace(/22/gi,2).replace(/33/gi,3).replace(/44/gi,4).replace(/55/gi,5).replace(/66/gi,6);
            }
            number = number.replace(/\*/gi,"");
	    data_sel[place] = $.grep(data_sel[place],function(n,i){//从选中数组中删除取消的号码
		return n == number;
	    },true);
	    if( !isButton ){//如果不是按钮触发则进行统计，按钮的统一进行统计
		checkNum();
	    }
	};
	//选择与取消号码选择交替变化
	function changeNoCss(obj){
	    if( $.trim($(obj).attr("class")) == 'selected' ){//如果本身是已选中，则做取消
		unSelectNum(obj,false);
	    }else{
		selectNum(obj,false);
	    }
	};
	//选择奇数号码
	function selectOdd(obj){
	    if( Number($(obj).html()) % 2 == 1 ){
		selectNum(obj,true);
	    }else{
		unSelectNum(obj,true);
	    }
	};
	//选择偶数号码
	function selectEven(obj){
	    if( Number($(obj).html()) % 2 == 0 ){
		selectNum(obj,true);
	    }else{
		unSelectNum(obj,true);
	    }
	};
	//选则大号
	function selectBig(i,obj){
	    if( i >= opts.noBigIndex ){
		selectNum(obj,true);
	    }else{
		unSelectNum(obj,true);
	    }
	};
	//选择小号
	function selectSmall(i,obj){
	    if( i < opts.noBigIndex ){
		selectNum(obj,true);
	    }else{
		unSelectNum(obj,true);
	    }
	};
        //选择随机
        function selectRandom() {
            var mname = $.lt_method[$.lt_method_data.methodid];//玩法的简写,如:'ZX3'
            var repeate = 0;
            var repeatetmp = [];
            if (otype == 'input') {//输入框形式的检测
                nums = parseInt(mname.charAt(mname.length - 1));
                var s = "";
                var repeatetmp = [];
                switch (mname) {//根据玩法分类不同做不同处理
                    case 'ETHDX' ://二同号单选
                        nums = 2;
                        break;
                    case 'SBTH': //三不同号
                        nums = 3;
                        break;
                    case 'EBTH': //二不同号
                        nums = 2;
                        break;
                    default     : //默认情况,有多少位选择多少位
                        break;
                }
                if (mname == 'ETHDX') {
                    var tmp = Math.floor(2 * Math.random());
                }
                for (var j = 0; j < nums; j++) {
                    if (repeate == 0) {//号码不允许重复
                        do {
                            var index = Math.floor(7 * Math.random());
                            if (!repeatetmp.contains(index) && index != 0) {
                                repeatetmp.push(index);
                                break;
                            }
                        } while (1);
                    } else {
                        var index = Math.floor(7 * Math.random());
                    }
                    s = s + index;
                    if (mname == 'ETHDX' && tmp == j) {
                        s = s + index;
                    }
                }
                data_sel[0] = s.split("|");
	    } else {//其他选择号码形式[暂时就数字型和大小单双]
                $.each($("div[name^='lt_place_']"), function(i, n) {
                    unSelectNum(n, true);
                });
                var nums = max_place;
                var amin = [];
                switch (mname) {//根据玩法分类不同做不同处理
                    case 'ETHDX' ://二同号单选
                        nums = 1;
                        amin[0] = 1;
                        amin[1] = 1;
                        break;
                    case 'SBTH': //三不同号
                        amin[0] = 3;
                        break;
                    case 'SBTHDT'://三不同号胆拖
                        nums = 1;
                        amin[0] = 1;
                        amin[1] = 2;
                        break;
                    case 'EBTH': //二不同号
                        amin[0] = 2;
                        break;
                    case 'EBTHDT'://二不同号胆拖
                        nums = 1;
                        amin[0] = 1;
                        amin[1] = 1;
                        break;
                    default     : //默认情况,有多少位选择多少位
                        for (var j = 0; j <= nums; j++) {
                            amin[j] = 1;
                        }
                        break;
                }
                var repeatetmp = [];
                for (var j = 0; j <= nums; j++) {
                    var objs = $("div[name^='lt_place_" + j + "']");
                    for (var k = 0; k < amin[j]; k++) {
                        if (repeate == 0) {//号码不允许重复
                            do {
                                var index = Math.floor(objs.length * Math.random());
                                var robj = $(objs[index]);
                                var number = $.trim($(robj).html());
                                //替换号码中的无用字符
                                number = number.replace(/\<span.*\<\/span>/gi, "").replace(/\r\n/gi, "");
                                number = number.replace(/\<div.*>(.*)\<\/div>/gi, "$1").replace(/\r\n/gi, "");
                                number = number.replace(/111/gi, 1).replace(/222/gi, 2).replace(/333/gi, 3).replace(/444/gi, 4).replace(/555/gi, 5).replace(/666/gi, 6);
                                if (methodname != "KSHZ" && methodname != "SBTHHZ") {//不是和值
                                    number = number.replace(/11/gi, 1).replace(/22/gi, 2).replace(/33/gi, 3).replace(/44/gi, 4).replace(/55/gi, 5).replace(/66/gi, 6);
                                }
                                number = number.replace(/\*/gi, "");
                                if (!repeatetmp.contains(number)) {
                                    repeatetmp.push(number);
                                    break;
                                }
                            } while (true);
                        } else {
                            var index = Math.floor(objs.length * Math.random());
                        }
                        var robj = $(objs[index]);
                        selectNum(robj, true);
                        objs.splice(index, 1);
                    }
                }
            }
            checkNum();
            $($.lt_id_data.id_sel_insert).click();
        }
        //随机一注
        $($.lt_id_data.id_random_one).unbind("click").click(function() {
            var tmpnumber = $($.lt_id_data.id_sel_num).html();
            if (tmpnumber > 0) {
                $.confirm(lot_lang.am_s38, function() {
                    selectRandom();
                }, function() {
                    $($.lt_id_data.id_sel_insert).click();
                }, '', 350);
            } else {
                selectRandom();
            }
        });
        //随机五注
        $($.lt_id_data.id_random_five).unbind("click").click(function() {
            var tmpnumber = $($.lt_id_data.id_sel_num).html();
            if (tmpnumber > 0) {
                $.confirm(lot_lang.am_s38, function() {
                    for (var j = 0; j < 5; j++) {
                        selectRandom();
                    }
                }, function() {
                    $($.lt_id_data.id_sel_insert).click();
                }, '', 350);
            } else {
                for (var j = 0; j < 5; j++) {
                    selectRandom();
                }
            }
        });

    //设置号码事件，手机端选号 老k
    mui(".ballGroup").on('tap',"div[name^='lt_place_']",function(){
        changeNoCss(this);
        $("li[class^='dxjoq']",$(this).closest("div[class='nbs']")).attr("class","dxjoq");
    })
	//设置号码事件
	// $(this).find("div[name^='lt_place_']").click(function(){
	//     changeNoCss(this);
	//     $("li[class^='dxjoq']",$(this).closest("div[class='nbs']")).attr("class","dxjoq");
	// });
	//全大小单双清按钮的动作行为处理
	if( opts.isButton == true || otype == 'dxds' ){//如果有这几个按钮才做处理
	    $("li[class='dxjoq']",$(this)).click(function(){//全大小基偶清
		$("li[class^='dxjoq']",$(this).parent()).attr("class","dxjoq");
		$(this).attr("class","dxjoq on");
		switch( $(this).attr('name') ){
		    case 'all'   :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    selectNum(n,true);
			});
			break;
		    case 'big'   :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    selectBig(i,n);
			});
			break;
		    case 'small' :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    selectSmall(i,n);
			});
			break;
		    case 'odd'   :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    selectOdd(n);
			});
			break;
		    case 'even'  :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    selectEven(n);
			});
			break;
		    case 'clean' :
			$.each($(this).closest("div[class='nbs']").find("div[name^='lt_place_']"),function(i,n){
			    unSelectNum(n,true);
			});
		    default :
			break;
		}
		checkNum();
	    });
	}
	//倍数输入处理事件
	$($.lt_id_data.id_sel_times).keyup(function(){
	    var times = $(this).val().replace(/[^0-9]/g,"").substring(0,5);
	    if( times == "" ){
		times = 0;
	    }else{
		times = parseInt(times,10);//取整倍数
	    }
	    var nums  = parseInt($($.lt_id_data.id_sel_num).html(),10);//投注注数取整
	    var modes = parseInt($($.lt_id_data.id_sel_modes).val(),10);//投注模式
	    var money = Math.round(times * nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//倍数*注数*单价 * 模式
	    money = isNaN(money) ? 0 : money;
	    $($.lt_id_data.id_sel_money).html(money);
		$(this).val( times );
	});
	$($.lt_id_data.id_sel_times).nextAll("a").click(function(){
	    $($.lt_id_data.id_sel_times).val($(this).html()).keyup();
	});
        //减少倍数
        $($.lt_id_data.id_reduce_times).unbind("click").click(function(){
            var times = Math.round(parseInt($($.lt_id_data.id_sel_times).val(),10));
            if(times < 1){
                times = 1;
            }
            $($.lt_id_data.id_sel_times).val(times).keyup();
        });
        //增加倍数
        $($.lt_id_data.id_plus_times).unbind("click").click(function(){
            var times = Math.round(parseInt($($.lt_id_data.id_sel_times).val(),10));
            if(times > 99999){
                times = 99999;
            }
            $($.lt_id_data.id_sel_times).val(times).keyup();
        });
	//模式变换处理事件
	$($.lt_id_data.id_sel_modes).change(function(){
	    var nums  = parseInt($($.lt_id_data.id_sel_num).html(),10);//投注注数取整
	    var times = parseInt($($.lt_id_data.id_sel_times).val(),10);//投注倍数取整
	    var modes = parseInt($($.lt_id_data.id_sel_modes).val(),10);//投注模式
	    var money = Math.round(times * nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//倍数*注数*单价 * 模式
	    money = isNaN(money) ? 0 : money;
	    $($.lt_id_data.id_sel_money).html(money);
	});
	//添加按钮
	$($.lt_id_data.id_sel_insert).unbind("click").click(function(){
	    var nums  = parseInt($($.lt_id_data.id_sel_num).html(),10);//投注注数取整
	    var times = parseInt($($.lt_id_data.id_sel_times).val(),10);//投注倍数取整
	    var modes = parseInt($($.lt_id_data.id_sel_modes).val(),10);//投注模式
	    var money = Math.round(times * nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//倍数*注数*单价 * 模式
	    var mid   = $.lt_method_data.methodid;
            var current_methodtitle = $.lt_method_data.title;//当前玩法组名称
            var current_methodname = $.lt_method_data.name;//当前玩法名称
	    if( isNaN(nums) || isNaN(times) || isNaN(money) || money <= 0 ){//如果没有任何投注内容
		$.alert(otype == 'input' ? lot_lang.am_s29 : lot_lang.am_s19);
		return;
	    }
	    if( otype == 'input' ){//如果是输入型，则检测号码合法性，以及是否存在重复号
		var mname = $.lt_method[mid];//玩法的简写,如:'ZX3'
		var error = [];
		var edump = [];
		var ermsg = "";
		//检测重复号，并除去重复号
		edump = dumpNum(true);
		if( edump.length >0 ){//有重复号
		    ermsg += lot_lang.em_s2+'\n'+edump.join(", ")+'\r&nbsp;';
		    checkNum();//重新统计
		    nums  = parseInt($($.lt_id_data.id_sel_num).html(),10);//投注注数取整
		    money = Math.round(times * nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//倍数*注数*单价*模式
		}
		switch(mname){//根据类型不同做不同检测
                    case 'SBTH'  ://二不同号单式
                        error = _inputCheck_Num(3,true,_SBTHDScheck,true);
                        break;
                    case 'EBTH'  ://三不同号单式
                        error = _inputCheck_Num(2,true,_EBTHDScheck,true);
                        break;
                    case 'ETHDX' ://二同号单选
                        error = _inputCheck_Num(3,true,_ETHDXcheck,true);
                        break;
                    default     :
                        break;
		}
		if( error.length > 0 ){//如果存在错误的号码，则提示
		    ermsg += lot_lang.em_s1+'\n'+error.join(", ")+'\r&nbsp;';
		}

		if( ermsg.length > 1 ){
		    $.alert("<div class='datainfo'>"+ermsg+"</div>",'','',400);
		}
	    }
	    var nos = $.lt_method_data.str;
	    var serverdata = "{'type':'"+otype+"','methodid':"+mid+",'codes':'";
	    var temp = [];
	    for( i=0; i<data_sel.length; i++ ){
                if( otype == 'input' ){
                    nos = nos.replace('X',data_sel[i].sort(_SortNum).join("|"));
                }else{
                   nos = nos.replace('X',data_sel[i].sort(_SortNum).join($.lt_method_data.sp.replace("s"," ")));
                }
		temp.push( data_sel[i].sort(_SortNum).join("&") );
	    }
	    if( nos.length > 40 ){
		var nohtml = nos.substring(0,35)+'...';
	    }else{
		var nohtml = nos;
	    }
	    //判断是否有重复相同的单
	    if( $.lt_same_code[mid] != undefined && $.lt_same_code[mid][modes] != undefined && $.lt_same_code[mid][modes].length > 0 ){
		if( $.inArray(temp.join("|"),$.lt_same_code[mid][modes]) != -1 ){//存在相同的
		    $.alert(lot_lang.am_s28);
		    return false;
		}
	    }
	    //计算动态奖金返点
	    var sel_isdy = false;
	    var sel_prize = 0;
	    var sel_point = 1;
	    if( $.lt_method_data.dyprize.length == 1 && $.lt_isdyna==1 ){//支持动态返点
		if( $('#lt_sel_dyprize') == undefined ){
		    $.alert(lot_lang.am_s27);
		    return false;
		}
		var sel_dy = $('#lt_sel_dyprize').val();
		sel_dy = sel_dy.split("|");
		if( sel_dy[1] == undefined ){
		    $.alert(lot_lang.am_s27);
		    return false;
		}
		sel_isdy = true;
		sel_prize = Math.round( Number(sel_dy[0]) * ($.lt_method_data.modes[modes].rate * 1000))/1000;
		sel_point = Number(sel_dy[1]);
	    }
            if($.lt_method_data.title == $.lt_method_data.name){
                noshtml = '['+$.lt_method_data.name+'] '+nohtml;
            }else{
                noshtml = '['+$.lt_method_data.title+'_'+$.lt_method_data.name+'] '+nohtml;
            }
            var myDate = new Date();
            var curTimes = myDate.getTime();
	    serverdata += temp.join("|")+"','nums':"+nums+",'times':"+times+",'money':"+money+",'mode':"+modes+",'point':'"+sel_point+"','desc':'"+noshtml+"','curtimes':'"+curTimes+"'}";
	    var cfhtml = '<tr style="cursor:pointer;"><td class="tl_li_l" width="4"></td><td>'+noshtml.substring(0,20)+'</td><td width=25>'+$.lt_method_data.modes[modes].name+'</td><td width=80 class="r">'+nums+lot_lang.dec_s1+'</td><td width=80 class="r">'+times+lot_lang.dec_s2+'</td><td width=120 class="r">'+money+lot_lang.dec_s3+'</td><td class="c tl_li_r" width="16"><input type="hidden" name="lt_project[]" value="'+serverdata+'" /></td></tr>';
	    var $cfhtml = $(cfhtml);
	    if( $.lt_total_nums == 0 ){
		$($.lt_id_data.id_cf_content).children().empty();
	    }
	    $cfhtml.prependTo($.lt_id_data.id_cf_content);
        mui('#popover').popover('show');
	    //详情查看
	    $('td[class="tl_li_l"]',$cfhtml).parent().mouseover(function(){
		var $h = $('<div class=fbox><table border=0 cellspacing=0 cellpadding=0><tr class=t><td class=tl></td><td class=tm></td><td class=tr></td></tr><tr class=mm><td class=ml><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td>'+lot_lang.dec_s30+': '+current_methodtitle+'_'+current_methodname+'<br/>'+lot_lang.dec_s31+': '+nohtml+'<br/>'+lot_lang.dec_s32+': '+$.lt_method_data.modes[modes].name+lot_lang.dec_s32+(sel_isdy ? (', '+lot_lang.dec_s33+' '+sel_prize+', '+lot_lang.dec_s34+' '+(Math.ceil(sel_point*1000)/10)+'%') : '')+'<br/><div class=in><span class=ic></span>  '+lot_lang.dec_s35+' '+nums+' '+lot_lang.dec_s1+', '+times+' '+lot_lang.dec_s2+', '+lot_lang.dec_s36+' '+money+' '+lot_lang.dec_s3+'</div></td><td class=mr><img src="'+pri_imgserver+'/images/comm/t.gif"></td></tr><tr class=b><td class=bl></td><td class=bm><img src="'+pri_imgserver+'/images/comm/t.gif"></td><td class=br></td></tr></table><div class=ar><div class=ic></div></div></div>');
		//var $h = $('<font>'+lot_lang.dec_s5+'</font><span class="close">['+lot_lang.dec_s6+']</span><br /><textarea class="code" readonly="readonly">'+nos+'</textarea>');
		var offset = $(this).offset();
		var left = offset.left+200;
		var top  = offset.top-79;
		$(this).openFloat($h,"more",left,top);
	    }).mouseout(function(){
		$(this).closeFloat();
	    }).click(function(){
		var sss = '<h4 style="text-align:left;">'+lot_lang.dec_s30+': '+current_methodtitle+'_'+current_methodname+'<br/>'+lot_lang.dec_s32+': '+$.lt_method_data.modes[modes].name+lot_lang.dec_s32+(sel_isdy ? (', '+lot_lang.dec_s33+' '+sel_prize+', '+lot_lang.dec_s34+' '+(Math.ceil(sel_point*1000)/10)+'%') : '')+'<br/>'+lot_lang.dec_s35+' '+nums+' '+lot_lang.dec_s1+', '+times+' '+lot_lang.dec_s2+', '+lot_lang.dec_s36+' '+money+' '+lot_lang.dec_s3+'</h4>';//+lot_lang.dec_s31+': <br/>';
		sss += '<div class="data" style="height:60px;"><table border=0 cellspacing=0 cellpadding=0><tr><td>'+nos+'</td></tr></table></div>';
		$.alert(sss,lot_lang.dec_s5,'',400,false);
	    });

	    $.lt_total_nums  += nums;//总注数增加
	    $.lt_total_money += money;//总金额增加
	    $.lt_total_money  = Math.round($.lt_total_money*1000)/1000;
	    basemoney         = Math.round(nums * 1 * ($.lt_method_data.modes[modes].rate * 1000))/1000;//注数*单价 * 模式
	    $.lt_trace_base   = Math.round(($.lt_trace_base+basemoney)*1000)/1000;
	    $($.lt_id_data.id_cf_num).html($.lt_total_nums);//更新总注数显示
	    $($.lt_id_data.id_cf_money).html($.lt_total_money);//更新总金额显示
	    $($.lt_id_data.id_cf_count).html(parseInt($($.lt_id_data.id_cf_count).html(),10)+1);//总投注项加1
	    //计算奖金，并且判断是否支持利润率追号
	    var pc = 0;
	    var pz = 0;
	    $.each($.lt_method_data.prize,function(i,n){
		n = isNaN(Number(n)) ? 0 : Number(n);
		pz = pz > n ? pz : n;
		pc++;
	    });
	    if( pc != 1 ){
		pz = 0;
	    }
	    pz = Math.round( pz * ($.lt_method_data.modes[modes].rate * 1000))/1000;
	    pz = sel_isdy ? sel_prize : pz;
	    $cfhtml.data('data',{
		methodid:mid,
		methodname:$.lt_method_data.title+'_'+$.lt_method_data.name,
		nums:nums,
		money:money,
		modes:modes,
		modename:$.lt_method_data.modes[modes].name,
		basemoney:basemoney,
		prize:pz,
		code:temp.join("|"),
		desc:nohtml
	    });
	    //把投注内容记录到临时数组中，用于判断是否有重复
	    if( $.lt_same_code[mid] == undefined ){
		$.lt_same_code[mid] = [];
	    }
	    if( $.lt_same_code[mid][modes] == undefined ){
		$.lt_same_code[mid][modes] = [];
	    }
	    $.lt_same_code[mid][modes].push(temp.join("|"));
	    $('td',$cfhtml).filter(".c").attr("title",lot_lang.dec_s24).click(function(){
		var n = $cfhtml.data('data').nums;
		var m = $cfhtml.data('data').money;
		var b = $cfhtml.data('data').basemoney;
		var c = $cfhtml.data('data').code;
		var d = $cfhtml.data('data').methodid;
		var f = $cfhtml.data('data').modes;
		var i = null;
		//移除临时数组中该投注内容，用于判断是否有重复
		$.each($.lt_same_code[d][f],function(k,code){
		    if( code == c ){
			i = k;
		    }
		});
		if( i != null ){
		    $.lt_same_code[d][f].splice(i,1);
		}else{
		    $.alert(lot_lang.am_s27);
		    return;
		}
		$.lt_total_nums  -= n;//总注数减少
		$.lt_total_money -= m;//总金额减少
		$.lt_total_money = Math.round($.lt_total_money*1000)/1000;
		$.lt_trace_base  = Math.round(($.lt_trace_base-b)*1000)/1000;
		$(this).parent().remove();
		if( $.lt_total_nums == 0 ){
		    $('<tr class="nr"><td class="tl_li_l" width="4"></td><td colspan="6" class="noinfo">暂无投注项</td><td class="tl_li_rn" width="4"></td></tr>').prependTo($.lt_id_data.id_cf_content);
		}
		$($.lt_id_data.id_cf_num).html($.lt_total_nums);//更新总注数显示
		$($.lt_id_data.id_cf_money).html($.lt_total_money);//更新总金额显示
		$($.lt_id_data.id_cf_count).html(parseInt($($.lt_id_data.id_cf_count).html(),10)-1);//总投注项减1
		cleanTraceIssue();//清空追号区数据
		if( $.lt_ismargin == false ){
		    traceCheckMarginSup();
		}
		//移除悬浮去
		$(this).parent().closeFloat();
	    });
	    //把所选模式存入cookie里面
	    SetCookie('modes',modes,86400);
	    SetCookie('dypoint',sel_point,86400);
        SetCookie('times',times,86400);
	    //成功添加以后清空选号区数据
	    for( i=0; i<data_sel.length; i++ ){//清空已选择数据
		data_sel[i] = [];
	    }
	    if( otype == 'input' ){//清空所有显示的数据
		$("#lt_write_box",$(me)).val("");
	    }
	    else if( otype == 'digital' || otype == 'dxds' || otype == 'dds' || otype == 'digitalts' ){
		$("div",$(me)).filter(".on").removeClass("on");
		$("li[class^='dxjoq']",$(me)).attr("class","dxjoq");
	    }
	    //还原倍数为1倍
	    $($.lt_id_data.id_sel_times).val(1);
	    checkNum();
	    //清空追号区数据
	    cleanTraceIssue();
	    //根据已投注内容决定是否保留利润率追号
	    if( $.lt_ismargin == true ){
		traceCheckMarginSup();
	    }
	});
    };

    //追号区
    $.fn.lt_trace = function(){
	var t_type  = 'margin';//追号形式[利润率:margin,同倍:same,翻倍:diff]
	$.extend({
	    lt_trace_issue: 0,//总追号期数
	    lt_trace_money: 0//总追号金额
	});
	var t_count = $.lt_issuecount;//可追号期数
	var currentendtime = new Date($.lt_end_time.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
	var t_nowpos= 0;//当前起始期在追号列表的位置[超过该位置的就不在处理,优化等待时间]
	//装载同倍、翻倍标签
	var htmllabel = '<span id="lt_margin" class="tab-front"><span class="tabbar-left"></span><span class="content">'+lot_lang.dec_s13+'</span><span class="tabbar-right"></span></span>';
	htmllabel += '<span id="lt_sametime" class="tab-back"><span class="tabbar-left"></span><span class="content">'+lot_lang.dec_s10+'</span><span class="tabbar-right"></span></span>';
	htmllabel += '<span id="lt_difftime" class="tab-back"><span class="tabbar-left"></span><span class="content">'+lot_lang.dec_s11+'</span><span class="tabbar-right"></span></span>';
	var htmltext  = '<span id="lt_margin_html">'+lot_lang.dec_s14+'&nbsp;<input name="lt_trace_times_margin" type="text" id="lt_trace_times_margin" value="1" size="3" />&nbsp;&nbsp;'+lot_lang.dec_s29+'&nbsp;<input name="lt_trace_margin" type="text" id="lt_trace_margin" value="50" size="3" />%&nbsp;&nbsp;</span>';
	htmltext += '<span id="lt_sametime_html" style="display:none;">'+lot_lang.dec_s14+'&nbsp;<input name="lt_trace_times_same" type="text" id="lt_trace_times_same" value="1" size="3" /></span>';
	htmltext += '<span id="lt_difftime_html" style="display:none;">'+lot_lang.dec_s17+'&nbsp;<input name="lt_trace_diff" type="text" id="lt_trace_diff" value="1" size="3" />&nbsp;'+lot_lang.dec_s18+'&nbsp;&nbsp;'+lot_lang.dec_s2+' '+lot_lang.dec_s19+' <input name="lt_trace_times_diff" type="text" id="lt_trace_times_diff" value="2" size="3" /></span>';
	htmltext += '&nbsp;&nbsp;'+lot_lang.dec_s15+'&nbsp;<input name="lt_trace_count_input" type="text" id="lt_trace_count_input" style="width:36px" value="10" size="3" /><input type="hidden" id="lt_trace_money" name="lt_trace_money" value="0" /><input type="hidden" id="lt_trace_alcount" />';
	$(htmllabel).appendTo($.lt_id_data.id_tra_label);
	$(htmltext).appendTo($.lt_id_data.id_tra_lhtml);
	//装载可追号期数
	$($.lt_id_data.id_tra_alct).val(t_count);
	$('#lt_margin').click(function(){//利润率
	    if( $(this).attr("class") != "tab-front" ){
		$(this).attr("class","tab-front");
		$('#lt_sametime').attr("class","tab-back");
		$('#lt_difftime').attr("class","tab-back");
		$('#lt_margin_html').show();
		$('#lt_sametime_html').hide();
		$('#lt_difftime_html').hide();
		t_type = 'margin';
                if ($($.lt_id_data.id_tra_if).attr("checked") == true) {//如果是选择了追号的情况才更新追号区
                    $($.lt_id_data.id_tra_ok).click();
                }
	    }
	});
	$('#lt_sametime').click(function(){//同倍
	    if( $(this).attr("class") != "tab-front" ){
		$(this).attr("class","tab-front");
		$('#lt_margin').attr("class","tab-back");
		$('#lt_difftime').attr("class","tab-back");
		$('#lt_margin_html').hide();
		$('#lt_sametime_html').show();
		$('#lt_difftime_html').hide();
		t_type = 'same';
                if ($($.lt_id_data.id_tra_if).attr("checked") == true) {//如果是选择了追号的情况才更新追号区
                    $($.lt_id_data.id_tra_ok).click();
                }
	    }
	});
	$('#lt_difftime').click(function(){//翻倍
	    if( $(this).attr("class") != "tab-front" ){
		$(this).attr("class","tab-front");
		$('#lt_margin').attr("class","tab-back");
		$('#lt_sametime').attr("class","tab-back");
		$('#lt_margin_html').hide();
		$('#lt_sametime_html').hide();
		$('#lt_difftime_html').show();
		t_type = 'diff';
                if ($($.lt_id_data.id_tra_if).attr("checked") == true) {//如果是选择了追号的情况才更新追号区
                    $($.lt_id_data.id_tra_ok).click();
                }
	    }
	});
	function upTraceCount(){//更新追号总期数和总金额
	    $('#lt_trace_count').html($.lt_trace_issue);
            if(parseInt($.lt_trace_issue,10) == 0){
                $("#lt_trace_qissueno").change();
            }else{
                $("#lt_trace_count_input").val($.lt_trace_issue);
            }
	    $('#lt_trace_hmoney').html(JsRound($.lt_trace_money,2,true));
	    $('#lt_trace_money').val($.lt_trace_money);
	}
	//标签内的输入框键盘事件
	$("input",$($.lt_id_data.id_tra_lhtml)).keyup(function(){
	    $(this).val( Number($(this).val().replace(/[^0-9]/g,"0")) );
	});
	//追号期快捷选择事件
	$("#lt_trace_qissueno").change(function(){
	    var t=0;
	    if($(this).val() == 'all' ){//全部可追号奖期
		t = parseInt($($.lt_id_data.id_tra_alct).val(),10);
	    }else{
		t = parseInt($(this).val(),10);
	    }
	    t = isNaN(t) ? 0 : t;
	    $("#lt_trace_count_input").val(t);
            $($.lt_id_data.id_tra_ok).click();
	});
	//装载追号的期号列表
	var issueshtml = '<table width="100%" cellspacing=0 cellpadding=0 border=0 id="lt_trace_issues_table">';
	var endtime = 0;
	var m = 0;
	$.each($.lt_issues,function(i,n){
	    endtime = new Date(n.endtime.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
	    if(m<t_count && endtime>=currentendtime){
		m++;
		issueshtml += '<tr id="tr_trace_'+n.issue+'"><td class="r1"><input type="checkbox" name="lt_trace_issues[]" value="'+n.issue+'" /></td><td>'+n.issue+'</td><td class="nosel"><input name="lt_trace_times_'+n.issue+'" type="text" class="r2" value="0" disabled/>'+lot_lang.dec_s2+'</td><td>'+lot_lang.dec_s20+'<span id="lt_trace_money_'+n.issue+'">0.00</span></td><td>'+n.endtime+'</td></tr>';
	    }
	});
	issueshtml += '</table>';
	$(issueshtml).appendTo($.lt_id_data.id_tra_issues);
	function changeIssueCheck(obj){//选中或者取消某期
	    var money = $.lt_trace_base;
	    var $j = $(obj).closest("tr");
	    if( $(obj).attr("checked") == true ){//选中
		$j.find("input[name^='lt_trace_times_']").val(1).attr("disabled",false).data("times",1);
		$j.find("span[id^='lt_trace_money_']").html(JsRound(money,2,true));
		$.lt_trace_issue++;
		$.lt_trace_money += money;
	    }else{  //取消选中
		var t =$j.find("input[name^='lt_trace_times_']").val();
		$j.find("input[name^='lt_trace_times_']").val(0).attr("disabled",true).data("times",0);
		$j.find("span[id^='lt_trace_money_']").html('0.00');
		$.lt_trace_issue--;
		$.lt_trace_money -= money*parseInt(t,10);
	    }
	    $.lt_trace_money = JsRound($.lt_trace_money,2);
	    upTraceCount();
	};
        $("#lt_trace_count_input").live("keyup", function() {//手动输入追号期数
            $($.lt_id_data.id_tra_ok).click();
        });
        $("#lt_trace_times_margin").live("keyup", function() {//利润率追号起始倍数
            $($.lt_id_data.id_tra_ok).click();
        });
        $("#lt_trace_margin").live("keyup", function() {//利润率
            $($.lt_id_data.id_tra_ok).click();
        });
        $("#lt_trace_times_same").live("keyup", function() {//同倍追号
            $($.lt_id_data.id_tra_ok).click();
        });
        $("#lt_trace_diff").live("keyup", function() {//翻倍追号
            $($.lt_id_data.id_tra_ok).click();
        });
        $("#lt_trace_times_diff").live("keyup", function() {//翻倍追号
            $($.lt_id_data.id_tra_ok).click();
        });
	$("input[name^='lt_trace_times_']",$($.lt_id_data.id_tra_issues)).live("keyup",function(){//每期的倍数变动
	    var v = Number($(this).val().replace(/[^0-9]/g,"0"));
	    $.lt_trace_money += $.lt_trace_base*(v-$(this).data('times'));
	    upTraceCount();
	    $(this).val(v).data("times",v);
	    $(this).closest("tr").find("span[id^='lt_trace_money_']").html(JsRound($.lt_trace_base*v,2,true));
	});
	$(":checkbox",$.lt_id_data.id_tra_issues).live("click",function(){//取消与选择某期
            changeIssueCheck(this);
            stopPropagation();
        });
	$("tr",$($.lt_id_data.id_tra_issues)).live("mouseover",function(){
	    $(this).attr("class","hv");
	}).live("mouseout",function(){
	    if( $(this).find(":checkbox").attr("checked") == false ){
		$(this).removeClass("hv");
	    }
	    else{
		$(this).attr("class","on");
	    }
	}).live("click",function(){
	    if( $(this).find(":checkbox").attr("checked") == false ){
		$(this).find(":checkbox").attr("checked",true);
	    }else{
		$(this).find(":checkbox").attr("checked",false);
	    }
	    changeIssueCheck($(this).find(":checkbox"));
	});
	$("input[name^='lt_trace_times_']",$($.lt_id_data.id_tra_issues)).live("click",function(){
            return false;
        });
	var _initTraceByIssue = function(){//根据起始期的不同重新加载追号区
	    var st_issue = $("#lt_issue_start").val();
	    cleanTraceIssue();//清空追号方案
	    var isshow   = false;//是否已经开始显示
	    var acount   = 0;//不可追号期统计
	    var loop     = 0;//循环次数
	    var mins     = t_nowpos;//开始处理的位置[初始为上次变更的位置]
	    var maxe     = t_nowpos;//结束处理的位置[初始为上次变更的位置]
	    var endtime = 0;
	    var k = 0;
	    var currentendtime = new Date($.lt_end_time.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
	    $.each($.lt_issues,function(i,n){
		endtime = new Date(n.endtime.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
		if(k<$.lt_issuecount && endtime>=currentendtime){
		    k++;
		    loop++;
		    if( isshow == false && st_issue == n.issue ){//如果选择的期数为当前期，则开始显示
			isshow = true;
		    }
		    if( isshow == false ){
			acount++;
			maxe = Math.max(maxe,acount);//取最大的位置
		    }else{
			mins = Math.min(mins,acount);//取最小位置
		    }
		    if( loop >= mins && loop <= maxe ){//如果没有超过要处理的最大数，则继续处理
			if( isshow == true ){//显示
			    $("#tr_trace_"+n.issue,$($.lt_id_data.id_tra_issues)).show();
			}else{//隐藏
			    $("#tr_trace_"+n.issue,$($.lt_id_data.id_tra_issues)).hide();
			}
		    }
		    if( loop > maxe ){//超过则退出不再处理
			return false;
		    }
		}
	    });
	    //更新可追号期数
	    t_count = $.lt_issuecount-acount;
	    if($("#lt_trace_qissueno").val()=='all'){
		$("#lt_trace_count_input").val(t_count);
	    }
	    t_nowpos = acount;
	    $($.lt_id_data.id_tra_alct).val(t_count);
	    //更新追号总期数和总金额
	    $.lt_trace_issue = 0;
	    $.lt_trace_money = 0;
	    upTraceCount();
	};
	//起始期变动对追号区的影响
	$("#lt_issue_start").change(function(){
	    if( $($.lt_id_data.id_tra_if).attr("checked") == true ){//如果是选择了追号的情况才更新追号区
		_initTraceByIssue();
	    }
	});
	//是否追号选择处理
	$($.lt_id_data.id_tra_if).attr("checked",false).click(function(){
	    if( $(this).attr("checked") == true ){
		//检测是否有投注内容
		if( $.lt_total_nums <= 0 ){
		    $.alert(lot_lang.am_s7);
		    $(this).attr("checked",false);
		    return;
		}
		$($.lt_id_data.id_tra_stop).attr("disabled",false).attr("checked",true);
		$($.lt_id_data.id_tra_box).show();
		_initTraceByIssue();
	    }else{
		$($.lt_id_data.id_tra_stop).attr("disabled",true).attr("checked",false);
		$($.lt_id_data.id_tra_box).hide();
	    }
	});
	//根据利润率计算当期需要的倍数[起始倍数，利润率，单倍购买金额，历史购买金额，单倍奖金],返回倍数
	var computeByMargin = function(s,m,b,o,p){
	    s = s ? parseInt(s,10) : 0;
	    m = m ? parseInt(m,10) : 0;
	    b = b ? Number(b) : 0;
	    o = o ? Number(o) : 0;
	    p = p ? Number(p) : 0;
	    var t = 0;
	    if( b > 0 ){
		if( m > 0 ){
		    t = Math.ceil( ((m/100+1)*o)/(p-(b*(m/100+1))) );
		}else{//无利润率
		    t = 1;
		}
		if( t < s ){//如果最小倍数小于起始倍数，则使用起始倍数
		    t = s;
		}
	    }
	    return t;
	};
	//立即生成按钮
	$($.lt_id_data.id_tra_ok).click(function(){
	    var c = parseInt($.lt_total_nums,10);//总投注注数
	    if( c <= 0 ){//无投注内容
		$.alert(lot_lang.am_s7);
		return false;
	    }
	    var p = 0;//奖金
	    if( t_type == 'margin' ){//如果为利润率追号则首先检测是否支持
		var marmt = 0;
		var marmd = 0;
		var martype =0;//利润率支持情况，0:支持,1:玩法不支持，2:多玩法，3:多圆角模式
		$.each($('tr',$($.lt_id_data.id_cf_content)),function(i,n){
		    if( marmt != 0 && marmt != $(n).data('data').methodid ){
			martype = 2;
			return false;
		    }else{
			marmt = $(n).data('data').methodid;
		    }
		    if( marmd != 0 && marmd != $(n).data('data').modes ){
			martype = 3;
			return false;
		    }else{
			marmd = $(n).data('data').modes;
		    }
		    if( $(n).data('data').prize <= 0 || (p!=0 && p != $(n).data('data').prize) ){
			martype = 1;
			return false;
		    }else{
			p = $(n).data('data').prize;
		    }
		});
		if( martype == 1 ){
		    $.alert(lot_lang.am_s32);
		    return false;
		}else if( martype == 2 ){
		    $.alert(lot_lang.am_s31);
		    return false;
		}else if( martype == 3 ){
		    $.alert(lot_lang.am_s33);
		    return false;
		}
	    }
	    var ic = parseInt($("#lt_trace_count_input").val(),10);//追号总期数
	    ic = isNaN(ic) ? 0 : ic;
	    if( ic <= 0 ){//期数没有填
		$.alert(lot_lang.am_s8);
		return false;
	    }
	    if( ic > $.lt_issuecount ){//超过可追号期数
		$.alert(lot_lang.am_s9,'','',300);
		return false;
	    }
	    var times = parseInt($("#lt_trace_times_"+t_type).val(),10);//倍数，当前追号方式里的倍数
	    times = isNaN(times) ? 0 : times;
	    if( times <= 0 ){
		$.alert(lot_lang.am_s10);
		return false;
	    }
	    times = isNaN(times) ? 0 : times;
	    var td = [];//根据用户填写的条件生成的每期数据
	    var tm = 0;//生成后的总金额
	    var msg='';//提示信息
	    if( t_type == 'same' ){
		var m = $.lt_trace_base*times;//每期金额
		tm = m*ic;//总金额=每期金额*期数
		for( var i=0; i<ic; i++ ){
		    td.push({
			times:times,
			money:m
		    });
		}
		msg = lot_lang.am_s12.replace("[times]",times);
	    }else if( t_type == 'diff' ){
		var d = parseInt($("#lt_trace_diff").val(),10);//相隔期数
		d = isNaN(d) ? 0 : d;
		if( d <= 0 ){
		    $.alert(lot_lang.am_s11);
		    return false;
		}
		var m = $.lt_trace_base;//每期金额的初始值
		var t = 1;//起始倍数为1
		for( var i=0; i<ic; i++ ){
		    if( i!= 0 && (i%d) == 0  ){
			t *= times;
		    }
		    td.push({
			times:t,
			money:m*t
			});
		    tm += m*t;
		}
		msg = lot_lang.am_s13.replace("[step]",d).replace("[times]",times);
	    }else if( t_type == 'margin' ){//利润追号
		var e = parseInt($("#lt_trace_margin").val(),10);//最低利润率
		e = isNaN(e) ? 0 : e;
		if( e <= 0 ){
		    $.alert(lot_lang.am_s30);
		    return false;
		}
		var m = $.lt_trace_base;//每期金额的初始值
		if( e >= ((p*100/m)-100) ){
		    $.alert(lot_lang.am_s30);
		    return false;
		}
		var t = 0;//返回的倍数
		for( var i=0; i<ic; i++ ){
		    t = computeByMargin(times,e,m,tm,p);
		    td.push({
			times:t,
			money:m*t
			});
		    tm += m*t;
		}
		msg = lot_lang.am_s34.replace("[margin]",e).replace("[times]",times);
	    }
	    msg += lot_lang.am_s14.replace("[count]",ic);
	    msg  = lot_lang.am_s99.replace("[msg]",msg);
	    //$.confirm(msg,function(){
            cleanTraceIssue();//清空以前的追号方案
            var $s = $("tr:visible", $($.lt_id_data.id_tra_issues));
            for (i = 0; i < ic; i++) {
                $($s[i]).find(":checkbox").attr("checked", true);
                $($s[i]).find("input[name^='lt_trace_times_']").val(td[i].times).attr("disabled", false).data("times", td[i].times);
                $($s[i]).find("span[id^='lt_trace_money_']").html(JsRound(td[i].money, 2, true));
                $($s[i]).addClass("on");
            }
            $.lt_trace_issue = ic;
            $.lt_trace_money = tm;
            upTraceCount();
            //},'','',350);
	});
    }

    //清空追号方案
    var cleanTraceIssue =function(){
	$("input[name^='lt_trace_issues']",$($.lt_id_data.id_tra_issues)).attr("checked",false);
	$("input[name^='lt_trace_times_']",$($.lt_id_data.id_tra_issues)).val(0).attr("disabled",true);
	$("span[id^='lt_trace_money_']",$($.lt_id_data.id_tra_issues)).html('0.00');
	$("tr",$($.lt_id_data.id_tra_issues)).removeClass("on");
	$('#lt_trace_hmoney').html(0);
	$('#lt_trace_money').val(0);
	$('#lt_trace_count').html(0);
	$.lt_trace_issue = 0;
	$.lt_trace_money = 0;
    };

    //根据投注内容决定是否保留利润率追号方式
    var traceCheckMarginSup = function(){
	var marmt = 0;
	var marmd = 0;
	var martype =0;//利润率支持情况，0:支持,1:玩法不支持，2:多玩法，3:多圆角模式
	var p = 0;//奖金
	if( $.lt_total_nums > 0 ){
	    $.each($('tr',$($.lt_id_data.id_cf_content)),function(i,n){
		if( marmt != 0 && marmt != $(n).data('data').methodid ){
		    martype = 2;
		    return false;
		}else{
		    marmt = $(n).data('data').methodid;
		}
		if( marmd != 0 && marmd != $(n).data('data').modes ){
		    martype = 3;
		    return false;
		}else{
		    marmd = $(n).data('data').modes;
		}
		if( $(n).data('data').prize <= 0 || (p!=0 && p != $(n).data('data').prize) ){
		    martype = 1;
		    return false;
		}else{
		    p = $(n).data('data').prize;
		}
	    });
	}
	if( martype > 0 ){//不支持利润率追号
	    $.lt_ismargin = false;
	    //隐藏利润率追号方式[默认为同倍追号]
	    $("#lt_margin").hide();
	    $("#lt_margin_html").hide();
	    $('#lt_sametime').click();

	}
	else{
	    $.lt_ismargin = true;
	    //显示利润率追号方式[默认为利润率追号]
	    $("#lt_margin").show();
	    $("#lt_margin_html").show();
	    $('#lt_margin').click();
	}
	return true;
    }

    //倒计时
    $.fn.lt_timer = function(start,end){//服务器开始时间，服务器结束时间
	var me = this;
	if( start == "" || end == "" ){
	    $.lt_time_leave = 0;
	}else{
	    $.lt_time_leave = (format(end).getTime()-format(start).getTime())/1000;//总秒数
	}
	function fftime(n){
	    return Number(n)<10 ? ""+0+Number(n) : Number(n);
	}
	function format(dateStr){//格式化时间
	    return new Date(dateStr.replace(/[\-\u4e00-\u9fa5]/g, "/"));
	}
	function diff(t){//根据时间差返回相隔时间
	    return t>0 ? {
		day : Math.floor(t/86400),
		hour : Math.floor(t/3600),
		minute : Math.floor(t%3600/60),
		second : Math.floor(t%60)
	    } : {
		day:0,
		hour:0,
		minute:0,
		second:0
	    };
	}
	var firstTime = Math.ceil(Math.random()*(269-210)+210);
	var secondTime = Math.ceil(Math.random()*(89-30)+30);
	var timerno = window.setInterval(function(){
	    //分开用户的请求时间，在3分30秒至4分29秒的时间段内;30秒至1分29秒的时间段内随机读取服务器时间
	    if($.lt_time_leave > 0 && ($.lt_time_leave % firstTime == 0 || $.lt_time_leave == secondTime )){
		$.ajax({
		    type: 'POST',
		    URL : $.lt_ajaxurl,
		    timeout : 30000,
		    data: "lotteryid="+$.lt_lottid+"&issue="+$($.lt_id_data.id_cur_issue).html()+"&flag=gettime",
		    success : function(data){//成功
			data = parseInt(data,10);
			data = isNaN(data) ? 0 : data;
			data = data <= 0 ? 0 : data;
			$.lt_time_leave = data;
		    }
		});
	    }
	    if( $.lt_time_leave <= 0 ){//结束
		clearInterval(timerno);
		//进入开奖倒计时,只有上期正常获取到才继续下期获取
		if( $.lt_open_status == true ){
		    $("#lt_opentimeleft").lt_opentimer($($.lt_id_data.id_cur_end).html(),$.lt_open_time,$($.lt_id_data.id_cur_issue).html());
		}
		if( $.lt_submiting == false ){//如果没有正在提交数据则弹出对话框,否则主动权交给提交表单
		    $.unblockUI({
			fadeInTime: 0,
			fadeOutTime: 0
		    });
		    $.confirm(lot_lang.am_s99.replace("[msg]",lot_lang.am_s15),function(){//确定
			$.lt_reset(false);
			$.lt_ontimeout();
		    },function(){//取消
			$.lt_reset(true);
			$.lt_ontimeout();
		    },'',450);
		    var confirm_no_txt = $("#confirm_no").val();
		    var auto_close_seconds = 3; // 奖期投注倒计时截止时弹出框自动关闭秒数
		    var auto_close_timerno = window.setInterval(function(){
		        if(auto_close_seconds <= 0) {
		            clearInterval(auto_close_timerno);
		            $("#confirm_no").click();
		        }
		        $("#confirm_no").val(confirm_no_txt + " (" + auto_close_seconds + ")");
		        auto_close_seconds--;
		    }, 1000);
		}
	    }
        var oDate = diff($.lt_time_leave--);
        var ahour = fftime(oDate.hour).toString().split("");
        var aminute = fftime(oDate.minute).toString().split("");
        var asecond = fftime(oDate.second).toString().split("");
        if (ahour[0] != $(me).find(".forward-h1").find("span").html()) {
            $(me).find(".forward-h1").html('<span>'+ahour[0]+'</span>');
        }
        if (ahour[1] != $(me).find(".forward-h2").find("span").html()) {
            $(me).find(".forward-h2").html('<span>'+ahour[1]+'</span>');
        }
        if (aminute[0] != $(me).find(".forward-m1").find("span").html()) {
            $(me).find(".forward-m1").html('<span>'+aminute[0]+'</span>');
        }
        if (aminute[1] != $(me).find(".forward-m2").find("span").html()) {
            $(me).find(".forward-m2").html('<span>'+aminute[1]+'</span>');
        }
        if (asecond[0] != $(me).find(".forward-s1").find("span").html()) {
            $(me).find(".forward-s1").html('<span>'+asecond[0]+'</span>');
        }
        if (asecond[1] != $(me).find(".forward-s2").find("span").html()) {
            $(me).find(".forward-s2").html('<span>'+asecond[1]+'</span>');
        }
    }, 1000);
    };
    //开奖倒计时
    $.fn.lt_opentimer = function(start,end,openissue){//服务器开始时间，服务器结束时间
	var me = this;
	if( start == "" || end == "" ){
	    var cc = 0;
	}else{
	    var cc = (format(end).getTime()-format(start).getTime())/1000;//总秒数
	}
	$.lt_time_open = Math.floor(cc);//开奖倒计时
	function fftime(n){
	    return Number(n)<10 ? ""+0+Number(n) : Number(n);
	}
	function format(dateStr){//格式化时间
	    return new Date(dateStr.replace(/[\-\u4e00-\u9fa5]/g, "/"));
	}
	function diff(t){//根据时间差返回相隔时间
	    return t>0 ? {
		day : Math.floor(t/86400),
		hour : Math.floor(t%86400/3600),
		minute : Math.floor(t%3600/60),
		second : Math.floor(t%60)
	    } : {
		day:0,
		hour:0,
		minute:0,
		second:0
	    };
	}
	$.lt_open_status = false;
	function _getcode(issue){
	    $.ajax({
		type: 'POST',
		url : $.lt_ajaxurl,
		data: "lotteryid="+$.lt_lottid+"&flag=gethistory&issue="+issue,
		success: function(data){
		    var partn = /<script.*>.*<\/script>/;
		    if( data == 'empty' || partn.test(data) ){
			return;
		    }
		    eval("data="+data);
		    $.lt_open_status = true;
		    var codebox = $("#showcodebox").find("li");
                    var $i = codebox.length - 1;
                    clearInterval(opentimerget);
                    var opencodeno = window.setInterval(function() {
                        if ($i < 0) {
                            clearInterval(moveno);
                            clearInterval(opencodeno);
                        }
                        $(codebox[$i]).attr("flag", "normal");
                        $(codebox[$i]).html(data.code[$i]);
                        $i--;
                    }, 500);
		    $("#lt_opentimebox").hide();
		    $("#lt_opentimebox2").hide();
		    $("#lt_gethistorycode").html(data.issue);
                    var $sRecentlyCode = "";
                    var recentlycode = data.recentlycode;
                    var issuecount = recentlycode.length;
                    for (var m = 0; m < issuecount; m++) {
                        $sRecentlyCode += '<tr><td style="text-align: center;">第' + recentlycode[m].issue + '期:</td>&nbsp;&nbsp';
                        var allcode = recentlycode[m].allcode;
                        var codecount = allcode.length;
                        $sRecentlyCode += '<td>';
                        for (var n = 0; n < codecount; n++) {
                            $sRecentlyCode +=  allcode[n] ;
                        }
                        $sRecentlyCode += '</td></tr>';
                    }
                    $("#nb-box2").html($sRecentlyCode);
                }
            });
        }
	$('#lt_gethistorycode').html(openissue);
	$("#lt_opentimebox").show();
	$("#lt_opentimebox2").hide();
	var _getTimes = 0;
	window.setTimeout(function(){
	    var tttime = Math.ceil(Math.random()*15+10)*1000;//10-25秒之间读取
	    opentimerget = window.setInterval(function(){
		if( $.lt_open_status == true || _getTimes > 20 ){
		    if( _getTimes > 20 ){
			$("#lt_opentimebox2").html("<strong>&nbsp;开奖超时,请刷新</strong>").show();
		    }
		    clearInterval(moveno);
                    $.each($("#showcodebox").find("li"),function(i,n){
                        $(this).attr("flag","normal");
                    });
                    clearInterval(opentimerget);
		}
		_getTimes++;
		_getcode($('#lt_gethistorycode').html());
	    },tttime);
	},cc*1000);
	var opentimerno = window.setInterval(function(){
	    if( $.lt_time_open <= 0 ){//结束
		clearInterval(opentimerno);
		$("#lt_opentimebox").hide();
		$("#lt_opentimebox2").show();
                $("#showcodebox").find("li").attr("flag","move");
                moveno = window.setInterval(function(){
                    $.each($("#showcodebox").find("li"),function(i,n){
                        if($(this).attr("flag") == "move"){
                            $(this).html(Math.floor(5*Math.random()+1));
                        }
                    });
                },40);
	    }
            $("#showcodebox").find("li").html("?");
	    var oDate = diff($.lt_time_open--);
            if($.lt_time_open < 60){
                $("#waitopendesc").html("开奖倒计时:");
                $(me).html(""+(oDate.hour>0 ? oDate.hour+":" : "")+fftime(oDate.minute)+":"+fftime(oDate.second));
            }else{
                $("#waitopendesc").html("等待开奖中...");
                $(me).html("");
            }
	},1000);
    };

    //根据投单完成和本期销售时间结束，进行重新更新整个投注界面
    $.lt_reset = function(iskeep){
	if( iskeep && iskeep === true ){
	    iskeep = true;
	}else{
	    iskeep = false;
	}
	if( $.lt_time_leave <= 0 ){//本期结束后的刷新
	    //01:刷新选号区
	    if( iskeep == false ){
		$("span[id^='smalllabel_'][class='tab-front']",$($.lt_id_data.id_smalllabel)).removeData("ischecked").click();
	    }
	    //02:刷新确认区
	    if( iskeep == false ){
		$.lt_total_nums  = 0;//总注数清零
		$.lt_total_money = 0;//总金额清零
		$.lt_trace_base  = 0;//追号基础数据
		$.lt_same_code   = [];//已在确认区的数据
		$($.lt_id_data.id_cf_num).html(0);//显示数据清零
		$($.lt_id_data.id_cf_money).html(0);//显示数据清零
		$($.lt_id_data.id_cf_content).children().empty();
		$('<tr class="nr"><td class="tl_li_l" width="4"></td><td colspan="6" class="noinfo">暂无投注项</td><td class="tl_li_rn" width="4"></td></tr>').prependTo($.lt_id_data.id_cf_content);
		$($.lt_id_data.id_cf_count).html(0);
		if( $.lt_ismargin == false ){
		    traceCheckMarginSup();
		}
	    }
	    //读取新数据刷新必须刷新的内容
	    $.ajax({
		type: 'POST',
		URL : $.lt_ajaxurl,
                timeout : 30000,
		data: "lotteryid="+$.lt_lottid+"&flag=read",
		success : function(data){//成功
		    if( data.length <= 0 ){
			$.alert(lot_lang.am_s16);
			return false;
		    }
		    var partn = /<script.*>.*<\/script>/;
		    if( partn.test(data) ){
			alert(lot_lang.am_s17,'','',300);
			top.location.href="./";
			return false;
		    }
		    if( data == "empty" ){
			alert(lot_lang.am_s18);
			window.location.href="./default_notice.shtml";
			return false;
		    }
		    eval("data="+data);
		    //03:刷新当前期的信息
		    $($.lt_id_data.id_cur_issue).html(data.issue);
		    $($.lt_id_data.id_cur_end).html(data.saleend);
		    $($.lt_id_data.id_cur_sale).html(data.sale);
		    $.lt_issuecount = data.left;
                    if(parseInt($("#lt_trace_count_input").val(),10) > $.lt_issuecount){
                        $("#lt_trace_count_input").val($.lt_issuecount);
                    }
		    //04:重新开始计时
		    $($.lt_id_data.id_count_down).lt_timer(data.nowtime, data.saleend);
		    $.lt_open_time = data.opentime;
		    $.lt_end_time = data.saleend;
		    //重新生成并写入起始期内容
		    var j = 0;
		    var endtime = 0;
		    var currentendtime = new Date($.lt_end_time.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
                    var chtml = '';
                    var lastissueshtml = '';
		    $.each($.lt_issues,function(i,n){
			endtime = new Date(n.endtime.replace(/[\-\u4e00-\u9fa5]/g, "/")).getTime();
			if(j<$.lt_issuecount && endtime>=currentendtime){
                            j++;
                            chtml += '<option value="'+n.issue+'">'+n.issue+(n.issue==data.issue?lot_lang.dec_s7:'')+'</option>';
                            lastissueshtml += '<tr id="tr_trace_'+n.issue+'"><td class="r1"><input type="checkbox" name="lt_trace_issues[]" value="'+n.issue+'" /></td><td>'+n.issue+'</td><td class="nosel"><input name="lt_trace_times_'+n.issue+'" type="text" class="r2" value="0" disabled/>'+lot_lang.dec_s2+'</td><td>'+lot_lang.dec_s20+'<span id="lt_trace_money_'+n.issue+'">0.00</span></td><td>'+n.endtime+'</td></tr>';
                        }
		    });
		    $("#lt_issue_start").empty();
		    $(chtml).appendTo("#lt_issue_start");
                    $("#lt_trace_issues_table").empty();
		    $(lastissueshtml).appendTo("#lt_trace_issues_table");
		    //06:更新可追号期数
		    t_count = $.lt_issuecount;
		    $($.lt_id_data.id_tra_alct).val(t_count);
		    //07:更新追号数据
		    cleanTraceIssue();//清空追号区数据
		},
		error : function(){//失败
		    $.alert(lot_lang.am_s16);
		    cleanTraceIssue();//清空追号区数据
		    return false;
		}
	    });
	}else{//提交表单成功后的刷新
	    //01:刷新选号区
	    if( iskeep == false ){
		$("span[id^='smalllabel_'][class='tab-front']",$($.lt_id_data.id_smalllabel)).removeData("ischecked").click();
	    }
	    //02:刷新确认区
	    if( iskeep == false ){
		$.lt_total_nums  = 0;//总注数清零
		$.lt_total_money = 0;//总金额清零
		$.lt_trace_base  = 0;//追号基数
		$.lt_same_code   = [];//已在确认区的数据
		$($.lt_id_data.id_cf_num).html(0);//显示数据清零
		$($.lt_id_data.id_cf_money).html(0);//显示数据清零
		$($.lt_id_data.id_cf_content).children().empty();
            $($.lt_id_data.id_sel_times).val(getCookie('times'));
		$('<tr class="nr"><td class="tl_li_l" width="4"></td><td colspan="6" class="noinfo">暂无投注项</td><td class="tl_li_rn" width="4"></td></tr>').prependTo($.lt_id_data.id_cf_content);
		$($.lt_id_data.id_cf_count).html(0);
		if( $.lt_ismargin == false ){
		    traceCheckMarginSup();
		}
	    }
	    //03:刷新追号区
	    if( iskeep == false ){
		cleanTraceIssue();//清空追号区数据
	    }
	}
    };
    //提交表单
    $.fn.lt_ajaxSubmit = function(){
	var me = this;
    mui("#popover").on('tap',$.lt_id_data.id_sendok,function(){
	// $(this).click(function(){
	    if( checkTimeOut() == false ){
		return;
	    }
	    $.lt_submiting = true;//倒计时提示的主动权转移到这里
	    var istrace = $($.lt_id_data.id_tra_if).attr("checked");
	    if( $.lt_total_nums <= 0 || $.lt_total_money <= 0 ){//检查是否有投注内容
		$.lt_submiting = false;
		$.alert(lot_lang.am_s7);
		return;
	    }
	    if( istrace == true ){
		if( $.lt_trace_issue <= 0 || $.lt_trace_money <= 0 ){//检查是否有追号内容
		    $.lt_submiting = false;
		    $.alert(lot_lang.am_s20);
		    return;
		}
		var terr = "";
		$("input[name^='lt_trace_issues']:checked",$($.lt_id_data.id_tra_issues)).each(function(){
		    if( Number($(this).closest("tr").find("input[name^='lt_trace_times_']").val()) <= 0 ){
			terr += $(this).val()+'&nbsp;&nbsp;';
		    }
		});
		if( terr.length > 0 ){//有错误倍数的奖期
		    $.lt_submiting = false;
		    $.alert(lot_lang.am_s21.replace("[errorIssue]",terr),'','',300,false);
		    return;
		}
	    }
	    if( istrace == true ){
		var msg = '<h4>'+lot_lang.am_s144.replace("[count]",$.lt_trace_issue)+'</h4>';
	    }else{
		var msg = '<h4>'+lot_lang.dec_s8.replace("[issue]",$("#lt_issue_start").val())+'</h4>';
	    }
	    msg += '<div class="data"><table border=0 cellspacing=0 cellpadding=0><tr class=hid><td width=115></td><td width=20></td><td></td></tr>';
	    var modesmsg = [];
	    var modes=0;
	    $.each($('tr',$($.lt_id_data.id_cf_content)),function(i,n){
		datas = $(n).data('data');
		msg += '<tr><td>'+datas.methodname+'</td><td>'+datas.modename+'</td><td>'+datas.desc+'</td></tr>';
	    });
	    msg += '</table></div>';
	    btmsg = '<div class="binfo"><span class=bbl></span><span class=bbm>'+(istrace==true ? lot_lang.dec_s16+': '+JsRound($.lt_trace_money,2,true) : lot_lang.dec_s9+': '+$.lt_total_money)+' '+lot_lang.dec_s3+'</span><span class=bbr></span></div>';
	    $.confirm(msg,function(){//点确定[提交]
		if( checkTimeOut() == false ){//正式提交前再检查以下时间
		    $.lt_submiting = false;
		    return;
		}
		$("#lt_total_nums").val($.lt_total_nums);
		$("#lt_total_money").val($.lt_total_money);
		ajaxSubmit();
	    },function(){//点取消
		$.lt_submiting = false;
		return checkTimeOut();
	    },'',370,true,btmsg);
	});
	//检查时间是否结束，然后做处理
	function checkTimeOut(){
	    if( $.lt_time_leave <= 0 ){//结束
		$.confirm(lot_lang.am_s99.replace("[msg]",lot_lang.am_s15),function(){//确定
		    $.lt_reset(false);
		    $.lt_ontimeout();
		},function(){//取消
		    $.lt_reset(true);
		    $.lt_ontimeout();
		},'',450);
		return false;
	    }else{
		return true;
	    }
	};
	//ajax提交表单
	function ajaxSubmit(){
	    $.blockUI({
		message: lot_lang.am_s22,
		overlayCSS: {
		    backgroundColor: '#000000',
		    opacity: 0.3,
		    cursor:'wait'
		}
	    });
	    var form = $(me).closest("form");
	    $.ajax({
		type: 'POST',
		url : $.lt_ajaxurl,
		timeout : 30000,
		data: $(form).serialize(),
		success: function(data){
		    //alert(data);
		    $.unblockUI({
			fadeInTime: 0,
			fadeOutTime: 0
		    });
		    $.lt_submiting = false;
		    //return false;
		    if( data.length <= 0 ){
			$.alert(lot_lang.am_s16);
			return false;
		    }
		    var partn = /<script.*>.*<\/script>/;
		    if( partn.test(data) ){
			alert(lot_lang.am_s17,'','',300);
			top.location.href="./";
			return false;
		    }
		    if( data == "success" ){//购买成功
			$.alert(lot_lang.am_s24,lot_lang.dec_s25,function(){
			    if( checkTimeOut() == true ){//时间未结束
				$.lt_reset();
			    }
			    $.lt_onfinishbuy();
                            $.fn.fastData();
                            $.fn.updatehistory();
			});
			return false;
		    }else{//购买失败提示
			eval("data = "+ data +";");
			if( data.stats == 'error' ){//错误
			    $.alert(lot_lang.am_s100+data.data,'',function(){
                                $.fn.updatehistory();
				return checkTimeOut();
			    },(data.data.length>10? 350 : 250));
			    return false;
			}
			if( data.stats == 'fail' ){//有失败的
			    msg = '<h4>'+lot_lang.am_s26+'</h4>';
			    //msg += lot_lang.am_s25.replace("[success]",data.data.success).replace("[fail]",data.data.fail);
			    msg += '<div class="data"><table width="100%" border="0" cellspacing="0" cellpadding="0">';
			    $.each(data.data.content,function(i,n){
				msg += '<tr><td>'+n.desc+'</td><td width="30%">'+n.errmsg+'</td></tr>';
			    });
			    msg += '</table></div>';
			    btmsg = '<div class="binfo"><span class=bbl></span><span class=bbm>'+lot_lang.am_s25.replace("[success]",data.data.success).replace("[fail]",data.data.fail)+'</span><span class=bbr></span></div>';
			    //msg += lot_lang.am_s26;
			    $.confirm(msg,function(){//点确定[清空]
				if( checkTimeOut() == true ){//时间未结束
				    $.lt_reset();
				}
				$.lt_onfinishbuy();
                                $.fn.fastData();
                                $.fn.updatehistory();
			    },function(){//点取消
				$.lt_onfinishbuy();
                                $.fn.fastData();
                                $.fn.updatehistory();
                                return checkTimeOut();
			    },'',480,true,btmsg);
			}
		    }
		},
		error: function(){
		    $.lt_submiting = false;
		    $.unblockUI({
			fadeInTime: 0,
			fadeOutTime: 0
		    });
                    $.confirm(lot_lang.am_s99.replace("[msg]",lot_lang.am_s23),function(){//点确定[清空]
                        if( checkTimeOut() == true ){//时间未结束
                            $.lt_reset();
                        }
                        $.lt_onfinishbuy();
                        $.fn.fastData();
                        $.fn.updatehistory();
                    },function(){//点取消
                        $.lt_onfinishbuy();
                        $.fn.fastData();
                        $.fn.updatehistory();
                        return checkTimeOut();
                    },'',500,true);
                    return false;
		}
	    });
	};

    };
    //当有资金发生变化时刷新左边余额
    $.fn.fastData = function(){
        fastData();
    };
    //更新最新投注记录
    $.fn.updatehistory = function(){
        $.ajax({
            type: 'POST',
            URL : $.lt_ajaxurl,
            data: "lotteryid="+$.lt_lottid+"&flag=hisproject",
            success : function(data){//成功
                eval("data=" + data + ";");
                if( data.length <= 0 ){
                    $.alert(lot_lang.am_s16);
                    return false;
                }
                if (data.length > 0) {
                    $(".projectlist").empty();
                    var $shtml = "";
                    $.each(data, function(i, n) {
                        if (parseInt(n.iscancel, 10) != 0) {
                            $shtml += '<div class="mui-row">';
                        } else {
                            $shtml += '<div class="mui-row">';
                        }

                        $shtml += '<div class="mui-col-xs-3 projectinfo" rel="projectinfo" style="text-align: center;" id="'+n.projectid+'"><a href="#noticeArticlePopover"  title="查看投注详情" class="red" >'+ n.projectid+'</a></div>';
                        // $shtml += '<td><a href="javascript:"  title="查看投注详情" class="blue" rel="projectinfo">' + n.projectid + '</a></td>';
                        // $shtml += '<td>' + n.writetime + '</td>';
                        // $shtml += '<td>' + n.methodname + '</td>';
                        $shtml += '<div class="mui-col-xs-3" style="text-align: center;">' + n.issue + '</div>';
                        // $shtml += '<td>' + n.code + '</td>';
                        // $shtml += '<td>' + n.multiple + '</td>';
                        // $shtml += '<td>' + n.modes + '</td>';
                        $shtml += '<div class="mui-col-xs-3" style="text-align: center;">' + n.totalprice + '</div>';
                        // $shtml += '<td>' + n.bonus + '</td>';
                        $shtml += '<div class="mui-col-xs-3" style="text-align: center;">' + n.statusdesc + '</div>';
                        $shtml += '</div>';
                    });
                    $("#mobileuserplaylist").html($shtml);
                }
            },
            error: function(){
                $.alert(lot_lang.am_s16);
            }
        });
    }
})(jQuery);
