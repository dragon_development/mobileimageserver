var msgList = new Array();
var nowindex = 0;
$(function(){
    window.setInterval("getMsg()",60000);
});
function nextnotice(){
    if (nowindex >= 0 && nowindex < msgList.total){
        nowindex++;
        $("#msgtitle").html(msgList[nowindex].title);
        $("#msgcontent").html(msgList[nowindex].content);
        msgIsRead(msgList[nowindex].entry);
    }
}
function prenotice(){
    if (nowindex > 0 && nowindex <= msgList.total){
        nowindex--;
        $("#msgtitle").html(msgList[nowindex].title);
        $("#msgcontent").html(msgList[nowindex].content);
    }
}
function msgclose(){
    $("#msgbox").hide("slow");
}
function getMsg(){
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "./users_getmsg.shtml",
        success: function(data){
            if (data != ""){
                msgList = data;
                if (parseInt(msgList.total) > 0){
                    $("#msgtitle").html(msgList[0].title);
                    $("#msgcontent").html(msgList[0].content);
                    $("#msgbox").show("slow");
                    if (parseInt(msgList.total) == 1) {
                        $("#msgpre").hide();
                        $("#msgnext").hide();
                    } else {
                        $("#msgpre").show();
                        $("#msgnext").show();
                    }
                    msgIsRead(msgList[0].entry);
                }
            }
        }
    }); 
}
function msgIsRead(mid){
    if (mid > 0){
        $.ajax({
            type: "POST",
            data: "mid=" + mid,  
            url: "./users_msgisread.shtml",
            success: function(data){
            }
        }); 
    }
}