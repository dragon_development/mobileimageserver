//var scrolls = function() {
function scrolls() {
    function id(name) {
        return document.getElementById(name);
    }
    //遍历函数

    function each(arr, callback, thisp) {
        if (arr.forEach) {
            arr.forEach(callback, thisp);
        }
        else {
            for (var i = 0, len = arr.length; i < len; i++)
                callback.call(thisp, arr[i], i, arr);
        }
    }
    function fadeIn(elem) {
        setOpacity(elem, 0)
        for (var i = 0; i < 20; i++) {
            (function() {
                var pos = i * 5;
                setTimeout(function() {
                    setOpacity(elem, pos)
                }, i * 25);
            })(i);
        }
    }
    function fadeOut(elem) {
        for (var i = 0; i <= 20; i++) {
            (function() {
                var pos = 100 - i * 5;
                setTimeout(function() {
                    setOpacity(elem, pos)
                }, i * 25);
            })(i);
        }
    }
    // 设置透明度
    function setOpacity(elem, level) {
        if (elem.filters) {
            elem.style.filter = "alpha(opacity=" + level + ")";
        } else {
            elem.style.opacity = level / 100;
        }
    }
    return {
        //count:图片数量，wrapId:包裹图片的DIV,ulId:按钮DIV,	infoId：信息栏
        scroll: function(count, wrapId, ulId) {
            var self = this;
            var targetIdx = 0;      //目标图片序号
            var curIndex = 0;       //现在图片序号
            //添加Li按钮
            var frag = document.createDocumentFragment();
            this.num = [];    //存储各个li的应用，为下面的添加事件做准备
//			this.info=id(infoId);
            for (var i = 0; i < count; i++) {
                (this.num[i] = frag.appendChild(document.createElement("li"))).innerHTML = i + 1;
            }
            id(ulId).appendChild(frag);

            //初始化信息
            this.img = id(wrapId).getElementsByTagName("a");
//			this.info.innerHTML=self.img[0].firstChild.title;
            this.num[0].className = "on";
            //将除了第一张外的所有图片设置为透明
            each(this.img, function(elem, idx, arr) {
                if (idx != 0)
                    setOpacity(elem, 0);
            });

            //为所有的li添加点击事件
            each(this.num, function(elem, idx, arr) {
                elem.onclick = function() {
                    self.fade(idx, curIndex, ulId);
                    curIndex = idx;
                    targetIdx = idx;
                }
            });

            //自动轮播效果
            var itv = setInterval(function() {
                if (targetIdx < self.num.length - 1) {
                    targetIdx++;
                } else {
                    targetIdx = 0;
                }
                self.fade(targetIdx, curIndex, ulId);
                curIndex = targetIdx;
            }, 3000);
            id(ulId).onmouseover = function() {
                clearInterval(itv)
            };
            id(ulId).onmouseout = function() {
                itv = setInterval(function() {
                    if (targetIdx < self.num.length - 1) {
                        targetIdx++;
                    } else {
                        targetIdx = 0;
                    }
                    self.fade(targetIdx, curIndex, ulId);
                    curIndex = targetIdx;
                }, 3000);
            }
        },
        fade: function(idx, lastIdx, ulId) {
            if (idx == lastIdx)
                return;
            var self = this;
            fadeOut(self.img[lastIdx]);
            fadeIn(self.img[idx]);
            each(self.num, function(elem, elemidx, arr) {
                if (elemidx != idx) {
                    self.num[elemidx].className = '';
                } else {
                    id(ulId).style.background = "";
                    self.num[elemidx].className = 'on';
                }
            });
//			this.info.innerHTML=self.img[idx].firstChild.title;
        }
    }
}
;
$(document).ready(function() {
    $("a[rel='notice']").click(function() {
        $("a[rel='notice']").removeClass();
        $(this).attr("class", "act");
        $("div[id^='notice_content_']").hide();
        $("#notice_content_" + $(this).attr("id")).show();
        if (noticelist[$(this).attr("id")] > 0) {
            var msgScroll = msgscroll($(this).attr("id"), noticelist[$(this).attr("id")]);
        }
    });
    $("div[id^='notice_content_']").hide();
    $("a[rel='notice']:first").click();
});
var temp = new Array();
var stemp = new Array();
function msgscroll(id, count) {
    if (stemp[id] != 1) {
        var cname = "Wo_" + id;
        cname = new scrolls();
        temp[id] = cname;
        stemp[id] = 1;
        cname.scroll(count, "banner_list_" + id, "list_" + id);
    }
}
function windowHeight() {
    var de = document.documentElement;
    return self.innerHeight || (de && de.clientHeight) || document.body.clientHeight;
}
function windowWidth() {
    var di = document.documentElement;
    return self.innerWidth || (di && di.clientWidth) || document.body.clientWidth;
}
window.onload = window.onresize = function() {
    var wh = windowHeight();
    var ww = windowWidth();
    document.getElementById("rightcon").style.height = (wh - 41) + "px";
}