function getKeyBoard($objs){
    $nums='1 2 3 4 5 6 7 8 9 0';
    $chars='a b c d e f g h i j k l m n o p q r s t u v w x y z';
    //$randNums=getTheArrayItems($nums.split(" ")).join(" ");
    //$randChars=getTheArrayItems($chars.split(" ")).join(" ");
    $randChars=$chars;
    $randNums = $nums;
    $chars1=$randChars.substr(0,25);
    $chars2=$randChars.substr(25,26);
    var keyboardhtml = "";//虚拟键盘
    var currentid = $objs.attr("id");
    keyboardhtml = '<div class="keyboard '+currentid+'">';
    keyboardhtml += '<ul>';
    keyboardhtml += '<li class="numkey">';
    $randNums = $randNums.split("");
    $.each($randNums,function(i,n){
        if(n!=" "){
            keyboardhtml += '<div class="key">'+n+'</div>';
        }
    });
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '<li>';
    $chars1 = $chars1.split("");
    $.each($chars1,function(i,n){
        if(n!=" "){
            keyboardhtml += '<div class="key charkey">'+n+'</div>';
        }
    });
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '<li>';
    $chars2 = $chars2.split("");
    $.each($chars2,function(i,n){
        if(n!=" "){
            keyboardhtml += '<div class="key charkey">'+n+'</div>';
        }
    });
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '<li class="funtion">';
    keyboardhtml += '<div class="funkey shift">大小写</div><div class="funkey ok">确定</div><div class="funkey cancel">取消</div><div class="funkey back">后退</div><div class="funkey usekey" style="color:red;font-weight:bold;">键盘录入</div>';
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '</ul>';
    keyboardhtml += '</div>';
    $objs.parent().css("position","relative");
    $objs.parent().append($(keyboardhtml));
    setCss($objs);
    $objs.click(function(){
        $(".keyboard").not("."+currentid).hide();
        if($("."+currentid).css("display") == "none"){
            $("."+currentid).show();
        }else{
            $("."+currentid).hide();
        }
    });
    $("body").bind('click', function(e){
        if($(e.target).attr("id") == currentid || $(e.target).attr("class").indexOf(currentid) > 0
            || $(e.target).parent().attr("class").indexOf(currentid) > 0 || $(e.target).parent().parent().attr("class").indexOf(currentid) > 0
            || $(e.target).parent().parent().parent().attr("class").indexOf(currentid) > 0 ){
            return false;                
        }else {
            $("."+currentid).hide();
        }
    });
    $("."+currentid).find(".key").click(function(){
        $objs.val($objs.val()+$(this).html());
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $(this).addClass("on");
    });
    $("."+currentid).find(".ok").click(function(){
        $("."+currentid).hide();
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
    });
    $("."+currentid).find(".cancel").click(function(){
        $("."+currentid).hide();
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $objs.val("");
    });
    $("."+currentid).find(".shift").click(function(){
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $.each($(".charkey"),function(){
            if($("."+currentid).css("width") == "460px"){
                $(this).html($(this).html().toUpperCase());
            }else{
                $(this).html($(this).html().toLowerCase());
            }
        });
        if($("."+currentid).css("width") == "460px"){
            $("."+currentid).css("width","480px");
        }else{
            $("."+currentid).css("width","460px");
        }
    });
    $("."+currentid).find(".back").click(function(){
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $objs.val($objs.val().substr(0,$objs.val().length-1));
    });
    $("."+currentid).find(".usekey").click(function(){
        if($(this).html()=="键盘录入"){
            usekey($objs);
            $(this).html("鼠标录入");
        }else{
            setCss($objs);
            $(this).html("键盘录入");
        }
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
    });
}
function getNumKeyBoard($objs){
    $nums='1 2 3 4 5 6 7 8 9 0';
    //$randNums=getTheArrayItems($nums.split(" ")).join(" ");
    $randNums = $nums;
    var keyboardhtml = "";//虚拟键盘
    var currentid = $objs.attr("id");
    keyboardhtml = '<div class="numkeyboard '+currentid+'">';
    keyboardhtml += '<ul>';
    keyboardhtml += '<li class="numkey">';
    $randNums = $randNums.split("");
    $.each($randNums,function(i,n){
        if(n!=" "){
            keyboardhtml += '<div class="key">'+n+'</div>';
        }
    });
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '<li class="funtion">';
    keyboardhtml += '<div class="funkey ok">确定</div><div class="funkey cancel">取消</div><div class="funkey back">后退</div><div class="funkey usekey" style="color:red;font-weight:bold;">键盘录入</div>';
    keyboardhtml += '</li>';
    keyboardhtml += '<div class="clear"></div>';
    keyboardhtml += '</ul>';
    keyboardhtml += '</div>';
    $objs.parent().css("position","relative");
    $objs.parent().append($(keyboardhtml));
    setCss($objs);
    $objs.click(function(){
        $(".numkeyboard").not("."+currentid).hide();
        if($("."+currentid).css("display") == "none"){
            $("."+currentid).show();
        }else{
            $("."+currentid).hide();
        }
    });
    $("body").bind('click', function(e){
        if($(e.target).attr("id") == currentid || $(e.target).attr("class").indexOf(currentid) > 0
            || $(e.target).parent().attr("class").indexOf(currentid) > 0 || $(e.target).parent().parent().attr("class").indexOf(currentid) > 0
            || $(e.target).parent().parent().parent().attr("class").indexOf(currentid) > 0 ){
            return false;                
        }else {
            $("."+currentid).hide();
        }
    });
    $("."+currentid).find(".key").click(function(){
        $objs.val($objs.val()+$(this).html());
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $(this).addClass("on");
    });
    $("."+currentid).find(".ok").click(function(){
        $("."+currentid).hide();
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
    });
    $("."+currentid).find(".cancel").click(function(){
        $("."+currentid).hide();
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $objs.val("");
    });
    $("."+currentid).find(".back").click(function(){
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
        $objs.val($objs.val().substr(0,$objs.val().length-1));
    });
    $("."+currentid).find(".usekey").click(function(){
        if($(this).html()=="键盘录入"){
            usekey($objs);
            $(this).html("鼠标录入");
        }else{
            setCss($objs);
            $(this).html("键盘录入");
        }
        $(".key").removeClass("on");
        $(".funkey").removeClass("on");
    });
}
function setCss($objs){
    $objs.css("background","#eee");
    $objs.css("border","1px solid #ddd");
    $objs.attr("readonly","true");
}
function usekey($objs){
    $objs.css("background","#FFF");
    $objs.css("border","1px solid #ddd");
    $objs.attr("readonly","");
}
function getTheArrayItems(arr) {
    var temp_array = new Array();
    var num=arr.length;
    for (var index in arr) {
        temp_array.push(arr[index]);
    }
    var return_array = new Array();
    for (var i = 0; i<num; i++) {
        if (temp_array.length>0) {
            var arrIndex = Math.floor(Math.random()*temp_array.length);
            return_array[i] = temp_array[arrIndex];
            temp_array.splice(arrIndex, 1);
        } else {
            break;
        }
    }
    return return_array;
}